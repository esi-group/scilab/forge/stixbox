// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - 1995 - Anders Holtsberg
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x,txt]=datas6()
  txt=['Cement';
'Source: Daniel, C. and Wood, F. S. (1980), Fitting Equations to Data: Computer Analysis of Multifactor Data, Second Edition, New York: John Wiley and Sons';
'Taken From: Chatterjee and Hadi (1988), p. 259.';
'Dimension:   14 observations on 6 variables';
'Description: The data set is one of a sequence of several data sets taken';
'  at different times in an experimental study relating the heat';
'  evolved during hardening of 14 samples of cement to the';
'       composition of the cement. The explanatory variables are';
'       weights (measured as percentages of the weight of each sample)';
'       of five Clinker compounds. The dependant variable (heat) is';
'       the last column in the data set.'];


 
 
x = [6,7,26,60,2.5,85.5
  15,1,29,52,2.2999999999999998,76
  8,11,56,20,5,110.40000000000001
  8,11,31,47,2.3999999999999999,90.599999999999994
  6,7,52,33,2.3999999999999999,103.5
  9,11,55,22,2.3999999999999999,109.8
  17,3,71,6,2.1000000000000001,108
  22,1,31,44,2.2000000000000002,71.599999999999994
  18,2,54,22,2.2999999999999998,97
  4,21,47,26,2.5,122.7
  23,1,40,34,2.2000000000000002,83.099999999999994
  9,11,66,12,2.6000000000000001,115.40000000000001
  8,10,68,12,2.3999999999999999,116.3
  18,1,17,61,2.1000000000000001,62.600000000000001];
 
endfunction

