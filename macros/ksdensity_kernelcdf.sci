// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function K=ksdensity_kernelcdf(varargin)
    // Evaluate kernel for CDF
    //  
    // Calling Sequence
    //   K=ksdensity_kernelcdf(u)
    //   K=ksdensity_kernelcdf(u,akernel)
    //
    // Parameters
    // u : a npoints-by-1 matrix of doubles, the points to evaluate the kernel
    // akernel : a 1-by-1 matrix of strings, the type of kernel (default="normal"). Available values are "normal", "biweight", "triangle", "epanechnikov".
    // K : a npoints-by-1 matrix of doubles, the kernel value
    //
    // Description
    // Evaluate the kernel for the CDF at point u. 
    //
    // See the description of the kernels in ksdensity_kernelpdf.
    // 
    // Examples
    // // Plot available kernels 
    // scf();
    // // 
    // subplot(2,2,1)
    // title("Normal")
    // u=linspace(-3,3,100);
    // K=ksdensity_kernelcdf(u,"normal");
    // plot(u,K,"r-")
    // // 
    // subplot(2,2,2)
    // title("Epanechnikov")
    // u=linspace(-1.5,1.5,100);
    // K=ksdensity_kernelcdf(u,"epanechnikov");
    // plot(u,K,"r-")
    // // 
    // subplot(2,2,3)
    // title("Biweight")
    // u=linspace(-1.5,1.5,100);
    // K=ksdensity_kernelcdf(u,"biweight");
    // plot(u,K,"r-")
    // // 
    // subplot(2,2,4)
    // title("Triangle")
    // u=linspace(-1.5,1.5,101);
    // K=ksdensity_kernelcdf(u,"triangle");
    // plot(u,K,"r-")
    // // 
    // // Plot available kernels 
    // scf();
    // xlabel("u");
    // ylabel("K(u)");
    // u=linspace(-1.5,1.5,101);
    // K=ksdensity_kernelcdf(u,"normal");
    // plot(u,K,"m-")
    // K=ksdensity_kernelcdf(u,"epanechnikov");
    // plot(u,K,"b--")
    // K=ksdensity_kernelcdf(u,"biweight");
    // plot(u,K,"r-.")
    // K=ksdensity_kernelcdf(u,"triangle");
    // plot(u,K,"g-")
    // legend(["Normal","Epanechnikov","Biweight","Triangle"]);
    //
    // Authors
    // Copyright (C) 2013 - Michael Baudin
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Kernel_(statistics)#Kernel_functions_in_common_use
    
    [lhs,rhs]=argn()
    apifun_checkrhs ( "ksdensity_kernelcdf" , rhs , 1:2 )
    apifun_checklhs ( "ksdensity_kernelcdf" , lhs , 0:1 )
    //
    u=varargin(1)
    akernel=apifun_argindefault(varargin,2,"normal")
    //
    // Check type
    apifun_checktype ( "ksdensity_kernelcdf" , u , "u" , 1 , "constant" )
    apifun_checktype ( "ksdensity_kernelcdf" , akernel , "kernel" , 2 , "string" )
    //
    // Check size
    // Do not check u : can be matrix or vector
    apifun_checkscalar ( "ksdensity_kernelcdf" , akernel , "kernel" , 2 )
    //
    // Check content
    apifun_checkoption ( "ksdensity_kernelcdf" , akernel , "akernel" , 2 , ..
    ["normal","biweight","triangle","epanechnikov"] )
    // 
    if akernel=="normal" then
        // Normal
        K = distfun_normcdf(u,0,1);
    elseif akernel=="epanechnikov" then
        // Epanechnikov
        K = 0.25*(2+3*u-u.^3);
        K(u<-1)=0
        K(u>1)=1
    elseif akernel=="biweight" then
        // Biweight
        K = (15.0/16.0)*(u-2*u.^3/3.0+u.^5/5.0+8.0/15.0);
        K(u<-1)=0
        K(u>1)=1
    elseif akernel=="triangle" then
        // Triangular
        K = zeros(u);
        i = find(-1<u & u<=0);
		if i <> [] then
			K(i) = u(i) + 0.5*u(i).^2 + 0.5;
		end
        i = find(0<u & u<=1);
		if i <> [] then
			K(i) = u(i) - 0.5*u(i).^2 + 0.5;
		end
        K(u<-1)=0
        K(u>1)=1
    end
endfunction
