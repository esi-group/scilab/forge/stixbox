// Copyright (C) 2016 - 2017 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function b = glmfit(varargin)
    // Estimates the parameters of the GLM
    //
    // Calling Sequence
    // b = glmfit(X,y)
    // b = glmfit(X,y,b0)
    // b = glmfit(X,y,b0,maxCalls)
    // b = glmfit(X,y,b0,maxCalls,maxIter)
    // b = glmfit(X,y,b0,maxCalls,maxIter,verbose)
    //
    // Parameters
    // X : a n-par-m matrix of doubles, the inputs of the model
    // y : a n-par-1 matrix of doubles, the data
    // b0 : a (m+1)-par-1 matrix of doubles, the initial values of the parameters of the model (default is a vector of zeros)
    // maxCalls : a 1-par-1 matrix of doubles, the maximum number of calls to the likelihood (default nap=1000)
    // maxIter : a 1-par-1 matrix of doubles, the maximum number of iterations in the optimization (default nap=1000)
    // verbose : a 1-par-1 matrix of boolean, set to true to print intermediate messages (default verbose=%f)
    // b : a (m+1)-par-1 matrix of doubles, the parameters of the model
    //
    // Description
    // Estimates the parameters of the GLM by searching b which 
    // maximizes the likelihood of the data. 
    // This function sequentially calls glmlikelihood until it finds the 
    // maximum. 
    // It is based on the optimization algorithm available in the 
    // optim function.
    //
    // Examples
    // TODO
    //
    // Authors
    // Copyright (C) 2016 - 2017 - Michael Baudin

    function [f, g, ind]=optimcostf(b,ind,X,y)
        global _GLMFIT_DATA_
        if (ind == 1)
            _GLMFIT_DATA_.niter = _GLMFIT_DATA_.niter + 1;
        end
        _GLMFIT_DATA_.nfevals = _GLMFIT_DATA_.nfevals + 1;
        //mprintf("Niter=%d, nfevals=%d\n", _GLMFIT_DATA_.niter,_GLMFIT_DATA_.nfevals )

        // vraisemblance logistique
        f=-glmlikelihood(b,X,y)
        callablefunction=list(glmlikelihood,X,y)
        g = -numderivative(callablefunction, b)
        if (ind == 1) then
            mprintf("f(x) = %s, |g(x)|=%s\n", string(f), string(norm(g)))
        end
    endfunction

    X=varargin(1)
    y=varargin(2)
    dim=size(X,"c")
    b0default=zeros(dim+1,1)
    b0=apifun_argindefault(varargin,3,b0default)
    maxCalls=apifun_argindefault(varargin,4,1000)
    maxIter=apifun_argindefault(varargin,5,1000)
    verbose=apifun_argindefault(varargin,6,%f)    
    //
    // Check input arguments
    apifun_checkveccol ( "glmfit" , b0 ,   "b0" ,  1, dim+1)
    //
    global _GLMFIT_DATA_
    _GLMFIT_DATA_ = tlist (["GLMDATA", "niter", "nfevals"]);
    _GLMFIT_DATA_.niter = 0;
    _GLMFIT_DATA_.nfevals = 0;
    //
    // Check that the function works (for internal use only !)
    // ll=glmlikelihood(b0,X,y)
    // [f, g, ind]=optimcostf(b0,1,X,y)
    callablefunction=list(optimcostf,X,y)
    // Configure verbose level
    if (verbose) then
        imp=-1
    else
        imp=0
    end
    // Run optim
    [fopt, b] = optim(callablefunction, b0, "ar",maxCalls,maxIter, imp=imp)
endfunction

