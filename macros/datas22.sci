// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - 1995 - Anders Holtsberg
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x,txt]=datas22()
  txt=['Memory chips datas';
'Source: J. Milton, J. Arnold (1995), ""Introduction to Probability and statistics"", p 699, McGraw-Hill international editions';
'Taken From: source';
'Dimensions: 20 observations de 1 variable';
'Description: Un echantillon de 300 puces memoires a ete preleve';
'    chaque jour de production, pendant 20 jours, dans la ';
'    production d''une entreprise. On donne le nombre de puces';
'    defectueuses trouve dans chaque  echantillon.  ']


x=[	16;
	8;
	1;
	16;
	9;
	13;
	10;
	14;
	11;
	8;
	6;
	14;
	13;
	14;
	4;
	11;
	4;
	13;
	9;
	12];
endfunction
