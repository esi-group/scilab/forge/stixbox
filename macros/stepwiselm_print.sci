// Copyright (C) 2016 - 2017 - Michael Baudin

function str=stepwiselm_print(multiindices,inputlabels)
    // Print the linear model
    //
    // Calling Sequence
    // str=stepwiselm_print(multiindices,inputlabels)
    //
    // Parameters
    // multiindices : a nbcoeffs-by-m matrix of doubles, integer value, greater or equal than 0, the exponents
    // inputlabels : a 1-by-m matrix of doubles, integer value, greater or equal than 0, the exponents
    // str : a nbcoeffs-by-1 matrix of strings, the labels of each sub-model
    //
    // Description
    // Prints the linear model.
    //
    // Authors
    // Copyright (C) 2016 - 2017 - Michael Baudin

    [lhs, rhs] = argn();
    apifun_checkrhs ( "stepwiselm_print" , rhs , 2 : 2 )
    apifun_checklhs ( "stepwiselm_print" , lhs , 0:1 )
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "stepwiselm_print" , multiindices , "multiindices" ,  1 , "constant" )
    apifun_checktype ( "stepwiselm_print" , inputlabels , "inputlabels" ,  2 , "string" )
    // Check size
    nbinputs = size(multiindices,"c")
    apifun_checkdims ( "stepwiselm_print" , inputlabels , "inputlabels" ,  2 , [1,nbinputs] )
    // Check content
    apifun_checkflint ( "stepwiselm_print" , multiindices , "multiindices" ,  1 )
    apifun_checkgreq ( "stepwiselm_print" , multiindices , "multiindices" ,  1, 0 )
    //
    str = [];
    k=0;
    nbinputs=size(inputlabels,"c")
    nbcoeffs = size(multiindices,"r")
    for i=1:nbcoeffs
        k = k + 1;
        if (i==1) then
            str(k) = "1";
        else
            str(k) = "";
        end
        for j=1:nbinputs
            // exponent_ij : exposant du i-ème coefficient, de la j-ème variable d'entrée
            exponent_ij = multiindices(i,j);
            if (exponent_ij>0) then
                if (exponent_ij==1) then
                    exponent_str = "";
                else
                    exponent_str = "^" + string(exponent_ij);
                end
                monom_ij_str = "(" + inputlabels(j) + ")" + exponent_str;
                if (str(k)=="") then
                    multsymbol = "";
                else
                    multsymbol = "*";
                end
                str(k) = str(k) + multsymbol + monom_ij_str;
            end
        end
    end
endfunction
