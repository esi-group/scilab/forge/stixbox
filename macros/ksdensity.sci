// Copyright (C) 2013 - 2016 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - 1995 - Anders Holtsberg
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [f,xi,width]=ksdensity(varargin)
    // Kernel smoothing density estimate
    //  
    // Calling Sequence
    //   ksdensity(x)
    //   ksdensity(x,xi)
    //   ksdensity(x,xi,"kernel",akernel)
    //   ksdensity(x,xi,"npoints",npoints)
    //   ksdensity(x,xi,"support",support)
    //   ksdensity(x,xi,"width",width)
    //   ksdensity(x,xi,"func",func)
    //   f=ksdensity(...)
    //   [f,xi]=ksdensity(...)
    //   [f,xi,width]=ksdensity(...)
    //
    // Parameters
    // x : a n-by-1 matrix of doubles, the data
    // xi : a npoints-by-1 matrix of doubles, the points where the density is estimated
    // width : a 1-by-1 matrix of doubles, positive, the width of the kernel (default=Silverman's rule).
    // support : a 1-by-1 matrix of string, the support (default="unbounded"). If support is "unbounded", all real values are possible. If support is "positive", only positive values are possible. 
    // akernel : a 1-by-1 matrix of strings, the type of kernel (default="normal"). Available values are "normal", "biweight", "triangle", "epanechnikov".
    // npoints : a 1-by-1 matrix of doubles, positive, the number of points in the density estimate (default=100)
    // func : a string, the function to estimate (default="pdf"). Available values are "pdf" and "cdf". If func=="pdf", estimates the probability density function. If func=="cdf", estimates the cumulative distribution function. 
    // f : a npoints-by-1 matrix of doubles, the density estimate
    //
    // Description
    // Compute a kernel density estimate of the data. 
    // The density estimate f is evaluated at the points in xi.
    //
    // For i=1,...,npoints, the estimate for the PDF is :
    //
    // <latex>
    // f(i)= \frac{1}{nh} \sum_{j=1}^n K\left(\frac{xi(i)-x(j)}{h} \right)
    // </latex>
    //
    // where K is a kernel for PDFs. 
    // These kernels are available in the ksdensity_kernelpdf function.
    //
    // For i=1,...,npoints, the estimate for the CDF is :
    //
    // <latex>
    // f(i)= \frac{1}{n} \sum_{j=1}^n K\left(\frac{xi(i)-x(j)}{h} \right)
    // </latex>
    //
    // where K is a kernel for CDFs.
    // These kernels are available in the ksdensity_kernelcdf function.
    //
    // The Silverman rule for the width u of the kernel is:
    //
    // <latex>
    // u=1.06\hat{\sigma} n^{-1/5},
    // </latex>
    //
    // where <latex>\hat{\sigma}</latex> is the empirical standard 
    // deviation, and n is the size of the sample.
    //
    // On output, <literal>xi</literal> and <literal>f</literal> contain 
    // the kernel density estimate of the data. 
    // For i=1,2,...,npoints, f(i) is the estimate 
    // of the probability density at xi(i).
    //
    // More details on the kernels available in this function 
    // is provided in the help of ksdensity_kernel.
    //
    // Examples
    // X=distfun_normrnd(0,1,1000,1);
    // [f,xi,width]=ksdensity(X);
    // scf();
    // histo(X,[],%t);
    // plot(xi,f,"r-");
    // xtitle("Kernel density estimate","X","Density");
    // legend(["Data","PDF estimate"]);
    //
    // // Set the kernel width
    // X=distfun_normrnd(0,1,1000,1);
    // [f,xi,width]=ksdensity(X,"width",0.5);
    // scf();
    // histo(X,[],%t);
    // plot(xi,f,"r-");
    //
    // // Set the number of points
    // X=distfun_normrnd(0,1,1000,1);
    // [f,xi,width]=ksdensity(X,"npoints",500);
    // scf();
    // histo(X,[],%t);
    // plot(xi,f,"r-");
    //
    // // Set the kernel
    // scf();
    // X=distfun_normrnd(0,1,1000,1);
    // //
    // subplot(2,2,1);
    // histo(X,[],%t);
    // [f,xi,width]=ksdensity(X,"kernel","normal");
    // plot(xi,f,"r-");
    // xtitle("Gaussian Density Estimate","X","Density")
    // legend(["Data","PDF estimate"]);
    // //
    // subplot(2,2,2);
    // histo(X,[],%t);
    // [f,xi,width]=ksdensity(X,"kernel","epanechnikov");
    // plot(xi,f,"r-");
    // xtitle("Epanechnikov Density Estimate","X","Density")
    // legend(["Data","PDF estimate"]);
    // //
    // subplot(2,2,3);
    // histo(X,[],%t);
    // [f,xi,width]=ksdensity(X,"kernel","biweight");
    // plot(xi,f,"r-");
    // xtitle("Biweight Density Estimate","X","Density")
    // legend(["Data","PDF estimate"]);
    // //
    // subplot(2,2,4);
    // histo(X,[],%t);
    // [f,xi,width]=ksdensity(X,"kernel","triangle");
    // plot(xi,f,"r-");
    // xtitle("Triangular Density Estimate","X","Density")
    // legend(["Data","PDF estimate"]);
    //
    // // Set the kernel width
    // X=distfun_normrnd(0,1,1000,1);
    // scf();
    // //
    // [f,xi,width]=ksdensity(X,"width",0.1);
    // subplot(2,2,1)
    // histo(X,[],%t);
    // plot(xi,f,"r-");
    // xtitle("width=0.1")
    // //
    // [f,xi,width]=ksdensity(X,"width",0.25);
    // subplot(2,2,2)
    // histo(X,[],%t);
    // plot(xi,f,"r-");
    // xtitle("width=0.25")
    // //
    // [f,xi,width]=ksdensity(X,"width",0.5);
    // subplot(2,2,3)
    // histo(X,[],%t);
    // plot(xi,f,"r-");
    // xtitle("width=0.5")
    // //
    // [f,xi,width]=ksdensity(X,"width",1.);
    // subplot(2,2,4)
    // histo(X,[],%t);
    // plot(xi,f,"r-");
    // xtitle("width=1.")
    //
    // // Estimate CDF
    // X=distfun_normrnd(0,1,1000,1);
    // [f,xi,width]=ksdensity(X,"func","cdf","kernel","epanechnikov");
    // scf();
    // xlabel("x");
    // ylabel("P(X<x)");
    // X=gsort(X,"g","i");
    // n=size(X,"*");
    // p=(1:n)/n;
    // stairs(X,p);
    // plot(xi,f,"r-");
    // xtitle("Kernel CDF estimate","X","Density");
    // legend(["Data","CDF estimate"],2);
    //
    // Authors
    // Copyright (C) 2013 - 2016 - Michael Baudin
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    // Copyright (C) 1993 - 1995 - Anders Holtsberg

    [lhs,rhs]=argn()
    apifun_checkrhs ( "ksdensity" , rhs , 1:5 )
    apifun_checklhs ( "ksdensity" , lhs , 0:3 )
    //
    x=varargin(1)
    xi=apifun_argindefault(varargin,2,[])
    if (typeof(xi)=="string") then
        xi=[]
    end
    //
    n = size(x,"*");
    //
    //
    // First string arg. = start of options
    startOptions=0
    for i=1:rhs
        if (typeof(varargin(i))=="string") then
            startOptions=i
            break
        end
    end
    if (startOptions==0) then
        startOptions=rhs+1
    end
    //
    // 1. Set the defaults
    default.kernel = "normal"
    default.npoints = 100
    default.support = "unbounded"
    udefault=1.0600000000000001*stdev(x)*(n^(-1/5));
    default.width = udefault
    default.func = "pdf"
    //
    // 2. Manage (key,value) pairs
    options=apifun_keyvaluepairs (default,varargin(startOptions:$))
    //
    // 3. Get parameters
    akernel=options.kernel
    npoints=options.npoints
    support = options.support
    width = options.width
    func = options.func
    //
    // Check Type
    apifun_checktype ( "ksdensity" , x , "x" , 1 , "constant" )
    if (xi<>[]) then
        apifun_checktype ( "ksdensity" , xi , "xi" , 2 , "constant" )
    end
    apifun_checktype ( "ksdensity" , akernel , "kernel" , startOptions , "string" )
    apifun_checktype ( "ksdensity" , npoints , "npoints" , startOptions , "constant" )
    apifun_checktype ( "ksdensity" , support , "support" , startOptions , "string" )
    apifun_checktype ( "ksdensity" , width , "width" , startOptions , "constant" )
    apifun_checktype ( "ksdensity" , func , "func" , startOptions , "string" )
    //
    // Check Size
    apifun_checkvector ( "ksdensity" , x , "x" , 1 )
    if (xi<>[]) then
        apifun_checkvector ( "ksdensity" , xi , "xi" , 2 )
    end
    apifun_checkscalar ( "ksdensity" , akernel , "kernel" , startOptions )
    apifun_checkscalar ( "ksdensity" , npoints , "npoints" , startOptions )
    apifun_checkscalar ( "ksdensity" , support , "support" , startOptions )
    apifun_checkscalar ( "ksdensity" , width , "width" , startOptions )
    apifun_checkscalar ( "ksdensity" , func , "func" , startOptions )
    //
    // Check Content
    apifun_checkoption ( "ksdensity" , akernel , "akernel" , startOptions , ..
    ["normal","biweight","triangle","epanechnikov"] )
    apifun_checkgreq ( "ksdensity" , npoints , "npoints" , startOptions,1)
    apifun_checkflint ( "ksdensity" , npoints , "npoints" , startOptions )
    tiny=number_properties("tiny")
    apifun_checkoption ( "ksdensity" , support , "support" , startOptions , ..
    ["unbounded","positive"] )
    apifun_checkgreq ( "ksdensity" , width , "width" , startOptions,tiny)
    apifun_checkoption ( "ksdensity" , func , "func" , startOptions , ..
    ["pdf","cdf"] )
    //
    x=x(:)
    //
    if (support=="positive"&or(x<0)) then
        error(msprintf(gettext("%s: There is a negative element in X."),"ksdensity"));
    end
    //
    // Get the points where to estimate the density : xi
    if (xi==[]) then
        xi=linspace(min(x),max(x),npoints)'
    end    
    // Compute the density estimate value at point xi, 
    // given data in x, 
    // with kernel "akernel" and with given width.
    n=size(x,"*")
    npoints=size(xi,"*")
    //
    // 1. Reshape x (the data) into a matrix, with 
    // one row for each point, and one column for each data
    x=repmat(x',npoints,1);
    //
    // 2. Reshape xi (the points where to evaluate f)
    // into a matrix, with 
    // one row for each point, and one column for each data
    xpoints=repmat(xi,1,n);
    //
    u = (xpoints-x)/width; // Standardized parameter
    if (func=="pdf") then
        K=ksdensity_kernelpdf(u,akernel); // Evaluate PDF Kernel
        f=sum(K,"c")/(n*width); // Sum
    else
        K=ksdensity_kernelcdf(u,akernel); // Evaluate CDF Kernel
        f=sum(K,"c")/n; // Sum
    end
    //
    if (support=="positive") then
        f(xi<0) = 0
    end
endfunction
