// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function boxcoxll = boxcoxloglikelihood(data,lambda)
    // Box-Cox log-likelihood
    //
    // Calling Sequence
    // boxcoxll = boxcoxloglikelihood(data,lambda)
    //
    // Parameters
    //   data : a m-by-1 matrix of doubles, greater or equal to zero, the data to transform
    //   lambda : a double, the lambda parameter
    //   boxcoxll : a double, the log-likelihood
    //
    // Description
    // boxcoxloglikelihood computes the log-likelihood of the 
    // data using the given value of lambda.
    //
    // Examples
    // data = [0.15 0.09 0.18 0.10 0.05 0.12 0.08];
    // lambda=0.7
    // boxcoxll = boxcoxloglikelihood(data,lambda)
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin


    [lhs, rhs] = argn()
    apifun_checkrhs ( "boxcoxloglikelihood" , rhs , 2 )
    apifun_checklhs ( "boxcoxloglikelihood" , lhs , 0:1 )
    //
    // Check Type
    apifun_checktype ( "boxcoxloglikelihood" , data , "data" , 1 , "constant" )
    apifun_checktype ( "boxcoxloglikelihood" , lambda , "lambda" , 2 , "constant" )
    //
    // Check Size
    apifun_checkvector ( "boxcoxloglikelihood" , data , "data" , 1 )
    apifun_checkscalar ( "boxcoxloglikelihood" , lambda , "lambda" , 1 )
    //
    // Check Content
    apifun_checkgreq ( "boxcoxloglikelihood" , data , "data" , 1 , 0 )
    //    
    transdat = boxcox(data,lambda)
    n=size(data,"*")
    v=sum((transdat-mean(transdat)).^2)
    boxcoxll = -0.5*n*log(v/n)+(lambda-1)*sum(log(data))
endfunction
