// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function varargout = boxcox(varargin)
    // Box-Cox transformation
    //
    // Calling Sequence
    //   [transdat,lambda]=boxcox(data)
    //   transdat=boxcox(data,lambda)
    //
    // Parameters
    //   data : a m-by-1 matrix of doubles, greater or equal to zero, the data to transform
    //   transdat : a m-by-1 matrix of doubles, the transformed data
    //   lambda : a m-by-1 matrix of doubles, the estimated lambda
    //
    // Description
    // [transdat,lambda]=boxcox(data) estimates the optimal lambda 
    // parameter and applies the Box-Cox transformation. 
    //
    // transdat=boxcox(data,lambda) applies the Box-Cox transformation 
    // using the given value of lambda. 
    //
    // The Box-Cox transformation is defined by 
    //
    //<latex>
    //\begin{eqnarray}
    //y=\frac{x^\lambda-1}{\lambda}
    //\end{eqnarray}
    //</latex>
    //
    // if lambda is nonzero and 
    //
    //<latex>
    //\begin{eqnarray}
    //y=\log(x)
    //\end{eqnarray}
    //</latex>
    //
    // if lambda=0.
    //
    // If lambda is not provided, the estimate of lambda is done with 
    // the boxcoxestimate function. 
    //
    // If the lambda parameter is estimated from the data, 
    // the transformed data has two interesting properties. 
    // First, the distribution of the transformed data has a variance which 
    // is closer to constant, i.e. it stabilizes the variance. 
    // Furthermore, the transformed data has a distribution which may be 
    // closer to the normal distribution. 
    //
    // This transformation can only be applied to positive data.
    //
    // Uses a robust implementation which is accurate 
    // even when lambda is close to zero. 
    //
    // Examples
    // data = [0.15 0.09 0.18 0.10 0.05 0.12 0.08];
    // lambda=0.7;
    // transdat=boxcox(data,lambda)
    // // Estimates lambda
    // [transdat,lambda]=boxcox(data)
    //
    // // See chi-square random numbers
    // data=distfun_chi2rnd(2,100,1);
    // scf();
    // subplot(1,2,1);
    // histo(data,"Normalization","pdf");
    // xlabel("Chi-Square Data");
    // ylabel("PDF");
    // // Applies Box-Cox : the transformed data 
    // // is closer to the normal distribution
    // [transdat,lambda]=boxcox(data)
    // subplot(1,2,2);
    // histo(transdat,"Normalization","pdf")
    // xlabel("BoxCox(Chi-Square Data)")
    // ylabel("PDF")
    // title(msprintf("Box-Cox with lambda=%.2f",lambda))
    //
    // // Check accuracy when lambda is close to zero
    // transdat=boxcox(0.15,1.e-20)
    // exact=-1.897119984885881302
    // 
    // Authors
    // Copyright (C) 2016 - Michael Baudin
    //
    // Bibliography
    // http://www.itl.nist.gov/div898/handbook/pmc/section5/pmc52.htm
    // https://en.wikipedia.org/wiki/Power_transform
    // https://www.unistat.com/guide/box-cox-regression/
    // http://www.stat.missouri.edu/~amicheas/stat7110/boxcox.html
    // http://robjhyndman.com/talks/RevolutionR/7-Transformations.pdf

    [lhs, rhs] = argn()
    apifun_checkrhs ( "boxcox" , rhs , 1:2 )

    //
    data = varargin ( 1 )
    if (rhs==1) then
        lambda = boxcoxestimate(data)
        apifun_checklhs ( "boxcox" , lhs , 2 )
    else
        lambda = varargin(2)
        apifun_checklhs ( "boxcox" , lhs , 1 )
    end
    //
    // Check Type
    apifun_checktype ( "boxcox" , data , "data" , 1 , "constant" )
    apifun_checktype ( "boxcox" , lambda , "lambda" , 2 , "constant" )
    //
    // Check Size
    apifun_checkvector ( "boxcox" , data , "data" , 1 )
    apifun_checkscalar( "boxcox" , lambda , "lambda" , 2 )
    //
    // Check Content
    tiniest=number_properties("tiniest")
    apifun_checkgreq ( "boxcox" , data , "data" , 1 , tiniest )
    //
    if (lambda==0.) then
        transdat=log(data)
    else
        transdat=specfun_expm1(lambda*log(data))/lambda;
    end
    //
    varargout(1)=transdat
    if (rhs==1) then
        varargout(2)=lambda
    end
endfunction
