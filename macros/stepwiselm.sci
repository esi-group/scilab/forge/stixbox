// Copyright (C) 2017 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function mdl = stepwiselm(varargin)
    // Stepwise model selection based on linear regression
    //
    // Calling Sequence
    // mdl = stepwiselm(y,x)
    // mdl = stepwiselm(y,x,direction)
    // mdl = stepwiselm(y,x,direction,criteria)
    // mdl = stepwiselm(y,x,direction,criteria,nitermax)
    // mdl = stepwiselm(y,x,direction,criteria,nitermax,verbose)
    //
    // Parameters
    // y : a m-by-1 matrix of doubles, the responses.
    // X : a m-by-n matrix of doubles, the inputs, where m is the number of observations and n is the number of variables.
    // direction : a string, the direction of search. Available values are "forward", "backward" and "both" (default direction = "forward").
    // criteria : a string, the criteria. Available values are "AIC" and "BIC" (default criteria = "BIC")
    // nitermax : a double, integer value, greater or equal to 1, the maximum number of iterations (default nitermax = 100)
    // verbose : a boolean, true if messages are to be printed (default verbose = %f)
    // mdl : a struct. See below for details
    //
    // Description
    // This function tries to find the model which maximizes the given criterion.
    //
    // The fields in mdl are the following. 
    // <itemizedlist>
    // <listitem> <para>
    // "selection" : a 1-by-n matrix of doubles, zeros or ones. Variable X(i) is selected if selection(i)==1.
    // </para> </listitem>
    // <listitem> <para>
    // "Z" : a m-by-nbselected matrix of doubles, the columns of the selected inputs
    // </para> </listitem>
    // <listitem> <para>
    // "selectionHistory" : a 1-by-nbselected matrix of doubles, the indices of the selected inputs, in the order where each variable was selected
    // </para> </listitem>
    // <listitem> <para>
    // "criteriaHistory" : a 1-by-nbselected matrix of doubles, the values of the criteria of the selected inputs, in the order where each variable was selected
    // </para> </listitem>
    // </itemizedlist>
    //
    // Examples
    // path = stixbox_getpath ();
    // csvfile = fullfile(path,"tests","unit_tests","regression","data_Linthurst-1979.csv");
    // separator = " ";
    // decimal = ".";
    // // BIO SAL pH K Na Zn
    // data = csvRead(csvfile, separator, decimal);
    // m = size(data,"r");
    // BIO = data(:,1);
    // SAL = data(:,2);
    // pH = data(:,3);
    // K  = data(:,4);
    // Na  = data(:,5);
    // Zn = data(:,6);
    // X = [ones(m,1) SAL pH K Na Zn];
    // y = BIO;
    // // With default parameters
    // mdl = stepwiselm(y,X)
    // // With "backward" and "AIC"
    // mdl = stepwiselm(y,X,"backward","AIC");
    // // With "both"
    // mdl = stepwiselm(y,X,"both");
    // // Print outputs
    // mdl = stepwiselm(y,X,"both",[],[],%t);
    //
    // // Search an order 2 model
    // modelspec = 2;
    // [X,multiindices]=stepwiselm_generate([SAL pH K Na Zn],modelspec);
    // mdl = stepwiselm(y,X,"forward","AIC");
    //
    // // Print the selection
    // inputlabels = ["SAL" "pH" "K" "Na" "Zn"];
    // // Apply the selection on the model
    // selectionindices = find(mdl.selection==1)';
    // multiindices = multiindices(selectionindices,:);
    // str = stepwiselm_print(multiindices,inputlabels);
    // disp(str)
    // // Use this selection to make a prediction
    // [B,bint,r,rint,stats,fullstats] = regress(y,mdl.Z);
    // yPredicted = mdl.Z*B;
    // // Make a plot to see the quality
    // scf();
    // plot(y,yPredicted,"b.")
    // plot(y,y,"r-")
    // xlabel("Observations")
    // ylabel("Predictions")
    // title(msprintf("R2=%.2f%%",100*fullstats.R2))
    //
    // Authors
    // Copyright (C) 2017 - Michael Baudin

    // Check number of input arguments
    [lhs,rhs] = argn();
    apifun_checkrhs("stepwiselm",rhs,2:6);
    apifun_checklhs("stepwiselm",lhs,0:1);
    //
    y = varargin(1)
    X = varargin(2)
    direction = apifun_argindefault ( varargin , 3 , "forward" )
    criteria = apifun_argindefault ( varargin , 4 , "BIC" )
    nitermax = apifun_argindefault ( varargin , 5 , 100 )
    verbose = apifun_argindefault ( varargin , 6 , %f )
    //
    // Check type
    apifun_checktype("stepwiselm",y,"y",1,"constant");
    apifun_checktype("stepwiselm",X,"X",2,"constant");
    apifun_checktype("stepwiselm",direction,"direction",3,"string");
    apifun_checktype("stepwiselm",criteria,"criteria",4,"string");
    apifun_checktype("stepwiselm",nitermax,"nitermax",5,"constant");
    //
    // Check size
    m = size(X,"r");
    apifun_checkdims("regress",y,"y",1,[m 1]);
    apifun_checkscalar("stepwiselm",direction,"direction",3);
    apifun_checkscalar("stepwiselm",criteria,"criteria",4);
    apifun_checkscalar("stepwiselm",nitermax,"nitermax",5);
    //
    // Check content
    apifun_checkoption("stepwiselm",direction,"direction",3,["forward","backward","both"]);
    apifun_checkoption("stepwiselm",criteria,"criteria",4,["BIC","AIC"]);
    apifun_checkflint("stepwiselm",nitermax,"nitermax",5);
    apifun_checkgreq("stepwiselm",nitermax,"nitermax",5,1);
    //
    if (direction=="forward") then
        [s,selectionHistory,criteriaHistory] = stepwiselm_forward(y,X,criteria,nitermax,verbose)
    elseif (direction=="backward") then
        [s,selectionHistory,criteriaHistory] = stepwiselm_backward(y,X,criteria,nitermax,verbose)
    else
        [s,selectionHistory,criteriaHistory] = stepwiselm_both(y,X,criteria,nitermax,verbose)
    end
    indices = find(s==1)
    Z = X(:,indices)
    mdl = struct("selection",s,"Z",Z,"selectionHistory",selectionHistory,"criteriaHistory",criteriaHistory)
endfunction

function [s,selectionHistory,criteriaHistory] = stepwiselm_forward(y,X,criteria,nitermax,verbose)
    n = size(X,"c")
    if (verbose) then
        mprintf("%s : Total number of candidates : %d\n","stepwiselm_forward",n)
    end
    s = zeros(1,n)
    // Always select the intercept
    s(1) = 1
    // Compute the initial criteria
    prevCrit = stepwiselm_criteria(y,X,s,criteria)
    selectionHistory = 1
    criteriaHistory = prevCrit
    // Loop over the iterations
    for i = 1:nitermax
        if (verbose) then
            mprintf("%s : Iter #%d. \n","stepwiselm_forward",i)
            selectedPredictors = find(s==1)
            selection_str = strcat(string(selectedPredictors)," ")
            nbSelectedPredictors = size(selectedPredictors,"*")
            mprintf("%s :     Selection size : %d \n","stepwiselm_forward",size(selectedPredictors,"*"))
            mprintf("%s :     Selection : %s \n","stepwiselm_forward",selection_str)
            mprintf("%s :     Criterion : %f \n","stepwiselm_forward",prevCrit)
        end
        // Loop over the predictors which have not been selected yet
        unselectedPredictors = find(s==0)
        // All predictors have already been selected, quit
        if (unselectedPredictors==[]) then
            if (verbose) then
                mprintf("%s :     All predictors already added- stop.\n","stepwiselm_forward")
            end
            break
        end
        // Compute the criteria for all candidate selections
        C = stepwiselm_forwardComp(y,X,s,unselectedPredictors,criteria,verbose)
        // The best predictor minimizes the criterion
        [bestCrit,bestPredictor] = min(C)
        if (verbose) then
            mprintf("%s :     Best predictor : %d, C=%f.\n","stepwiselm_forward",bestPredictor,bestCrit)
        end
        if (bestCrit>prevCrit) then
            // The criteria does not decrease anymore : quit
            if (verbose) then
                mprintf("%s :     Stop.\n","stepwiselm_forward")
            end
            break
        end
        // The criteria increases most when we add the component into the selection : add it !
        s(bestPredictor) = 1
        prevCrit = bestCrit
        if (verbose) then
            mprintf("%s :     Adding predictor %d.\n","stepwiselm_forward",bestPredictor)
        end
        selectionHistory($+1) = bestPredictor
        criteriaHistory($+1) = bestCrit
    end
endfunction

function C = stepwiselm_forwardComp(y,X,s,unselectedPredictors,criteria,verbose)
    // Forward algorithm
    // Compute the criteria for all candiate selections
    C = %inf * ones(1,n)
    for j = unselectedPredictors
        candidateselection = s
        candidateselection(j) = 1
        C(j) = stepwiselm_criteria(y,X,candidateselection,criteria)
    end
    if (verbose) then
        C_str = "[" + strcat(string(C),",") + "]"
        // mprintf("%s :     C=%s.\n","stepwiselm_forwardComp",C_str)
    end
endfunction

function [s,selectionHistory,criteriaHistory] = stepwiselm_backward(y,X,criteria,nitermax,verbose)
    n = size(X,"c")
    if (verbose) then
        mprintf("%s : Total number of candidates : %d\n","stepwiselm_backward",n)
    end
    // Start with all predictors selected
    s = ones(1,n)
    // Compute the initial criteria
    prevCrit = stepwiselm_criteria(y,X,s,criteria)
    selectionHistory = %inf
    criteriaHistory = prevCrit
    // Loop over the iterations
    for i = 1:nitermax
        if (verbose) then
            mprintf("%s : Iter #%d. \n","stepwiselm_backward",i)
            selectedPredictors = find(s==1)
            selection_str = strcat(string(selectedPredictors)," ")
            nbSelectedPredictors = size(selectedPredictors,"*")
            mprintf("%s :     Selection size : %d \n","stepwiselm_backward",size(selectedPredictors,"*"))
            mprintf("%s :     Selection : %s \n","stepwiselm_backward",selection_str)
            mprintf("%s :     Criterion : %f \n","stepwiselm_backward",prevCrit)
        end
        // Loop over the predictors which have already been selected
        selectedPredictors = find(s==1)
        // All predictors have already been removed, quit
        if (selectedPredictors==[]) then
            if (verbose) then
                mprintf("%s :     All predictors already removed - stop.\n","stepwiselm_backward")
            end
            break
        end
        // Compute the criteria for all candidate selections
        C = stepwiselm_backwardComp(y,X,s,selectedPredictors,criteria,verbose)
        // The worst predictor minimizes the criterion
        [worstCrit,worstPredictor] = min(C)
        if (verbose) then
            mprintf("%s :     Worst predictor : %d, C=%f.\n","stepwiselm_backward",worstPredictor,worstCrit)
        end
        if (worstCrit>prevCrit) then
            // The criteria does not decrease anymore : quit
            if (verbose) then
                mprintf("%s :     Stop.\n","stepwiselm_backward")
            end
            break
        end
        // The criteria increases most when we remove the component from the selection : remove it !
        s(worstPredictor) = 0
        prevCrit = worstCrit
        if (verbose) then
            mprintf("%s :     Removing predictor %d.\n","stepwiselm_backward",worstPredictor)
        end
        selectionHistory($+1) = -worstPredictor
        criteriaHistory($+1) = worstCrit
    end
endfunction

function C = stepwiselm_backwardComp(y,X,s,selectedPredictors,criteria,verbose)
    // Backward algorithm
    // Compute the criteria for all candiate selections
    C = %inf * ones(1,n)
    for j = selectedPredictors
        if (j==1) then
            // Never remove the intercept
            C(j) = %inf
        else
            candidateselection = s
            candidateselection(j) = 0
            C(j) = stepwiselm_criteria(y,X,candidateselection,criteria)
        end
    end
    if (verbose) then
        C_str = strcat(string(C),",")
        //mprintf("%s :     C=%s.\n","stepwiselm_backwardComp",C_str)
    end
endfunction

function crit = stepwiselm_criteria(y,X,candidateselection,criteria)
    // Compute the criteria for a candidate selection
    indices = find(candidateselection==1)
    Z = X(:,indices)
    LogLikelihood = regress_fast(y,Z);
    n = size(Z,"c");
    m = size(Z,"r");
    if (criteria=="BIC")
        crit = criterionBayesian(n,LogLikelihood,m);
    else
        crit = criterionAkaike(n,LogLikelihood);
    end
    // mprintf("#Var.=%d, Size=%d, Log(L)=%f, C=%f, P-value=%e\n",n,m,logL,crit,fullstats.pval)
endfunction

function LogLikelihood = regress_fast(y,X)
    // A shorter and faster version of Stixbox's regress
    //
    m = size(X,"r")
    //
    // Proceed...
    [U,S,V] = svd(X,"e");
    S = diag(S);
    tol = max(size(X)) * S(1) * %eps;
    // Rank
    rnk=size(find(S>tol),"*");
    if rnk == 0 then
        IA = zeros(X');
    else
        V = V(:,1:rnk);
        U = U(:,1:rnk);
        IS = diag(ones(rnk,1)./S(1:rnk));
        IA = V* IS *U';
    end
    B = IA*y;
    // The covariance matrix : C
    // CC is mathematically equal to inv(X'*X)
    CC = V*IS.^2*V'
    // Compute residuals
    r = y-X*B
    // Regression 
    ResidualSS = sum(r.^2)
    // Compute Log-Likelihood
    sigma2 = ResidualSS/m
    if (sigma2==0) then
        LogLikelihood = %nan
    else
        LogLikelihood = -0.5*m*(log(2*%pi*sigma2)+1)
    end
endfunction


function [s,selectionHistory,criteriaHistory] = stepwiselm_both(y,X,criteria,nitermax,verbose)
    n = size(X,"c")
    // TODO : add an initial selection
    if (verbose) then
        mprintf("%s : Total number of candidates : %d\n","stepwiselm_both",n)
    end
    // Start with half predictors selected
    s = ones(1,n)
    s(3:2:$) = 0
    // Compute the initial criteria
    prevCrit = stepwiselm_criteria(y,X,s,criteria)
    selectedPredictors = find(s==1)
    selectionHistory = selectedPredictors
    nbpredictors = size(selectedPredictors,"*")
    criteriaHistory = prevCrit(ones(1,nbpredictors))
    // Loop over the iterations
    for i = 1:nitermax
        if (verbose) then
            mprintf("%s : Iter #%d. \n","stepwiselm_both",i)
            selectedPredictors = find(s==1)
            selection_str = strcat(string(selectedPredictors)," ")
            nbSelectedPredictors = size(selectedPredictors,"*")
            mprintf("%s :     Selection size : %d \n","stepwiselm_both",nbSelectedPredictors)
            mprintf("%s :     Selection : %s \n","stepwiselm_both",selection_str)
            mprintf("%s :     Criterion : %f \n","stepwiselm_both",prevCrit)
        end
        //
        // Step 1 : evaluate candidate predictors to add
        unselectedPredictors = find(s==0)
        C_forward = stepwiselm_forwardComp(y,X,s,unselectedPredictors,criteria,verbose)
        [bestCrit,bestPredictor] = min(C_forward)
        if (verbose) then
            mprintf("%s :     Best predictor : %d, C=%f.\n","stepwiselm_both",bestPredictor,bestCrit)
        end
        //
        // Step 2 : evaluate candidate predictors to remove
        selectedPredictors = find(s==1)
        C_backward = stepwiselm_backwardComp(y,X,s,selectedPredictors,criteria,verbose)
        [worstCrit,worstPredictor] = min(C_backward)
        //
        // Step 3 : see if we add a predictor, remove a predictor or stop.
        if (verbose) then
            mprintf("%s :     Worst predictor : %d, C=%f.\n","stepwiselm_both",worstPredictor,worstCrit)
        end
        if ((bestCrit<prevCrit) | (worstCrit<prevCrit)) then
            if (bestCrit<worstCrit) then
                // The criteria increases most when we add the component 
                // into the selection : add it !
                s(bestPredictor) = 1
                prevCrit = bestCrit
                if (verbose) then
                    mprintf("%s :     Adding predictor %d.\n","stepwiselm_both",bestPredictor)
                end
                selectionHistory($+1) = bestPredictor
                criteriaHistory($+1) = bestCrit
            else
                // The criteria increases most when we remove the component 
                // from the selection : remove it !
                s(worstPredictor) = 0
                prevCrit = worstCrit
                if (verbose) then
                    mprintf("%s :     Removing predictor %d.\n","stepwiselm_both",worstPredictor)
                end
                selectionHistory($+1) = -worstPredictor
                criteriaHistory($+1) = worstCrit
            end
        else
            // The criteria does not decrease anymore : quit
            if (verbose) then
                mprintf("%s :     Stop.\n","stepwiselm_both")
            end
            break
        end
    end
endfunction
