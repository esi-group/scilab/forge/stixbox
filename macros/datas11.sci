// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - 1995 - Anders Holtsberg
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x,txt]=datas11()
  txt=['Demography';
'Source: Gunst, R. F., and Mason, R. L. (1980), Regression Analysis and Its Application: A Data-Oriented Approach, New York: Marcel Dekker, p. 358.';
'Taken From: Source';
'Dimension:   49 observations on 7 variables';
'Description: As described in Gunst and Mason, the data set is a subset of';
'  larger data set. The variables are demographic characteristics';
'  of the following 49 countries:';
'         1 Australia            2 Austria                 3 Barbados';
'         4 Belgium              5 British Guiana          6 Bulgaria';
'         7 Canada               8 Chile                   9 Costa Rica';
'        10 Cyprus              11 Czechoslovakia         12 Denmark';
'        13 El Salvador         14 Finland                15 France';
'        16 Guatemala           17 Hong Kong              18 Hungary';
'        19 Iceland             20 India                  21 Ireland';
'        22 Italy               23 Jamaica                24 Japan';
'        25 Luxembourg          26 Malaya                 27 Malta';
'        28 Mauritius           29 Mexico                 30 Netherlands';
'        31 New Zealand         32 Nicaragua              33 Norway';
'        34 Panama              35 Poland                 36 Portugal';
'        37 Puerto Rico         38 Romania                39 Singapore';
'        40 Spain               41 Sweden                 42 Switzerland';
'        43 Taiwan              44 Trinidad               45 United Kingdom';
'        46 United States       47 USSR                   48 West Germany';
'        49 Yugoslavia';
'Column   Description';
'  1      infant death per 1,000 live births';
'  2      # of inhabitants per physician';
'  3      population per square kilometer';
'  4      population per 1,000 hectars of agricultural land';
'  5      percentage literate of population aged 15 years and over';
'  6      # of students enrolled in higher education per 100,000 population';
'  7      gross national product per capita (1957 U.S. dollars)'];

x = [19.5,860,1,21,98.5,856,1316
  37.5,695,84,1720,98.5,546,670
  60.399999999999999,3000,548,7121,91.099999999999994,24,200
  35.399999999999999,819,301,5257,96.700000000000003,536,1196
  67.099999999999994,3900,3,192,74,27,235
  45.100000000000001,740,72,1380,85,456,365
  27.300000000000001,900,2,257,97.5,645,1947
  127.90000000000001,1700,11,1164,80.099999999999994,257,379
  78.900000000000006,2600,24,948,79.400000000000006,326,357
  29.899999999999999,1400,62,1042,60.5,78,467
  31,620,108,1821,97.5,398,680
  23.699999999999999,830,107,1434,98.5,570,1057
  76.299999999999997,5400,127,1497,39.399999999999999,89,219
  21,1600,13,1512,98.5,529,794
  27.399999999999999,1014,83,1288,96.400000000000006,667,943
  91.900000000000006,6400,36,1365,29.399999999999999,135,189
  41.5,3300,3082,98143,57.5,176,272
  47.600000000000001,650,108,1370,97.5,258,490
  22.399999999999999,840,2,79,98.5,445,572
  225,5200,138,2279,19.300000000000001,220,73
  30.5,1000,40,598,98.5,362,550
  48.700000000000003,746,164,2323,87.5,362,516
  58.700000000000003,4300,143,3410,77,42,316
  37.700000000000003,930,254,7563,98,750,306
  31.5,910,123,2286,96.5,36,1388
  68.900000000000006,6400,54,2980,38.399999999999999,475,356
  38.299999999999997,980,1041,8050,57.600000000000001,142,377
  69.5,4500,352,4711,51.799999999999997,14,225
  77.700000000000003,1700,18,296,50,258,262
  16.5,900,346,4855,98.5,923,836
  22.800000000000001,700,9,170,98.5,839,1310
  71.700000000000003,2800,10,824,38.399999999999999,110,160
  20.199999999999999,946,11,3420,98.5,258,1130
  54.799999999999997,3200,15,838,65.700000000000003,371,329
  74.700000000000003,1100,96,1411,95,351,475
  77.5,1394,100,1087,55.899999999999999,272,224
  52.399999999999999,2200,271,4030,81,1192,563
  75.700000000000003,788,78,1248,89,226,360
  32.299999999999997,2400,2904,108214,50,437,400
  43.5,1000,61,1347,87,258,293
  16.600000000000001,1089,17,1705,98.5,401,1380
  21.100000000000001,765,133,2320,98.5,398,1428
  30.5,1500,305,10446,54,329,161
  45.399999999999999,2300,168,4383,73.799999999999997,61,423
  24.100000000000001,935,217,2677,98.5,460,1189
  26.399999999999999,780,20,399,98,1983,2577
  35,578,10,339,95,539,600
  33.799999999999997,798,217,3631,98.5,528,927
  100,1637,73,1215,77,524,265];
endfunction
