// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function filledbounds(varargin) 
    // Fill the area between two curves
    // 
    // Calling Sequence
    // filledbounds(x,bounds)
    // filledbounds(...,"foreground",foregroundcolor)
    // filledbounds(...,"background",backgroundcolor)
    // 
    // Parameters
    // x : a n-by-1 matrix of doubles, the X-values
    // bounds : a n-by-2 matrix of doubles, the bounds. bounds(:,1) is the lower bound. bounds(:,2) is the upper bound. 
    // foregroundcolor : a 1-by-1 matrix of strings, the color of the line around the area (default=color("gray50"))
    // backgroundcolor : a 1-by-1 matrix of strings, the color inside the filled area (default=color("gray75"))
    // 
    // Description
    // When managing a data associated with an error, 
    // it is convenient to plot the lower and upper error bounds, and 
    // to fill the area in-between. 
    // When the data is depending on x, the error may also depend 
    // on x. 
    // filledbounds plots the lower and upper bounds, and 
    // paint the area between the two curves. 
    //
    // Implementation note
    //
    // Unfortunately, Scilab does not support transparency 
    // at this time. 
    // Hence, only one such filled curve can be plotted at once. 
    // Moreover, it must be the first on the plot, so that 
    // latter plots can be visible.
    //
    // Examples
    // // This is a function :
    // x=linspace(0,10)';
    // y= x.*sin(x);
    // // This is the noise (it depends on x):
    // sigma=abs(x);
    // low=y-1.96*sigma;
    // upp=y+1.96*sigma;
    // bounds=[low,upp];
    // filledbounds(x,bounds)
    // plot(x,y,"b-")
    // legend(["95% Conf. Inter","F"],"in_upper_left");
    // 
    // // Configure foreground
    // scf();
    // filledbounds(x,bounds,"foreground",5) // red
    // plot(x,y,"b-")
    // legend(["95% Conf. Inter","F"],"in_upper_left");
    //
    // // Configure background
    // scf();
    // filledbounds(x,bounds,"background",3) // green
    // plot(x,y,"b-")
    // legend(["95% Conf. Inter","F"],"in_upper_left");
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin

    [lhs,rhs] = argn();
    apifun_checkrhs("filledbounds",rhs,[2,4,6]);
    apifun_checklhs("filledbounds",lhs,0:1);
    //
    x=varargin(1)
    bounds=varargin(2)
    //
    // 1. Set the defaults
    default=struct(..
    "foreground",color("gray50"), ..
    "background",color("gray75"))
    //
    // 2. Manage (key,value) pairs
    options=apifun_keyvaluepairs (default,varargin(3:$))
    //
    // 3. Get parameters
    foreground = options.foreground
    background = options.background
    //
    //
    // Check type
    apifun_checktype("filledbounds",x,"x",1,"constant");
    apifun_checktype("filledbounds",bounds,"bounds",2,"constant");
    //
    // Check size
    apifun_checkvector("filledbounds",x,"x",1);
    n=size(x,"*")
    apifun_checkdims("filledbounds",bounds,"bounds",2,[n,2]);
    apifun_checkscalar("filledbounds",foreground,"foreground",3);
    apifun_checkscalar("filledbounds",background,"background",3);
    //
    // Get content : nothing to do
    //
    lowerbound=bounds(:,1)
    upperbound=bounds(:,2)
    xfpoly([x,x($:-1:1)],[lowerbound,upperbound($:-1:1)]); 
    epoly=gce(); 
    epoly.foreground=foreground; 
    epoly.background=background; 
    h=gcf()
    h.children.data_bounds(:,2)=[min(lowerbound);max(upperbound)]
endfunction
