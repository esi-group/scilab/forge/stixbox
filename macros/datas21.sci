// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - 1995 - Anders Holtsberg
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x,txt]=datas21()
  txt=['Bulbs';
'Source: ""Lighting Comes of Age with New Technology"" in Research and Development, Novembre 1992.';
'Taken From: J. Milton, J. Arnold (1995), ""Introduction to Probability and statistics"", p 215,  McGraw-Hill international editions';
'Dimensions: 24 observations de 1 variable';
'Description: Duree de vie (en milliers d''heures) d''un echantillon d''ampoules electriques de marque Philips.']


x=[9.1
   10.1
   9.0
   11.4
   10.5
   9.5
   12.0
   9.1
   12.2
   13.1
   10.0
   9.3
   9.0
   9.6
   11.1
   9.1
   13.3
   10.7
   9.1
   9.0
   9.0
   11.0
   9.2
   11.6];
endfunction
