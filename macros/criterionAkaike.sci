// Copyright (C) 2017 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function c = criterionAkaike(n,logL)
    // Akaike's Information Criterion for estimated model
    //
    // Calling Sequence
    //   c = criterionAkaike(n,logL)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, greater or equal to 1, the number of variables in the model
    // logL : a 1-by-1 matrix of doubles, integer value, the log-likelihood of the model vs the observations
    // c : a 1-by-1 matrix of doubles, the value of the criterion
    //
    // Description
    // This function computes Akaike's information criterion, i.e. AIC :
    //
    // <screen>
    // c = 2 n − 2 ln (L)
    // </screen>
    //
    // where L is the likelihood of the model vs the observations.
    //
    // If the model has a good fit, the criterion is close to zero 
    // (and positive).
    //
    // Examples
    // // A simple linear regression
    // X = [
    //     57.   
    //     64.   
    //     69.   
    //     82.   
    //     92.   
    //     111.  
    //     114.  
    //     132.  
    //     144.  
    //     146.  
    // ];
    // Y = [
    //     121.  
    //     129.  
    //     140.  
    //     164.  
    //     188.  
    //     217.  
    //     231.  
    //     264.  
    //     289.  
    //     294.  
    // ];
    // [B,bint,r,rint,stats,fullstats] = regress(Y,[ones(X),X])
    // logL = fullstats.LogLikelihood
    // n = size(X,"c")
    // c = criterionAkaike(n,logL)
    //
    // Authors
    // Copyright (C) 2017 - Michael Baudin

    // Check number of input arguments
    [lhs,rhs] = argn();
    apifun_checkrhs("criterionAkaike",rhs,2:2);
    apifun_checklhs("criterionAkaike",lhs,1:1);
    //
    // Check type
    apifun_checktype("criterionAkaike",n,"n",1,"constant");
    apifun_checktype("criterionAkaike",logL,"logL",2,"constant");
    //
    // Check size
    apifun_checkscalar("criterionAkaike",n,"n",1);
    apifun_checkscalar("criterionAkaike",logL,"logL",2);
    //
    // Check content
    apifun_checkflint("criterionAkaike",n,"n",1);
    apifun_checkgreq("criterionAkaike",n,"n",1,1);
    //
    c = 2 * n - 2 * logL
endfunction
