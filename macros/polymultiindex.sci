// Copyright (C) 2013 - 2017 - Michael Baudin

function a=polymultiindex(p,d)
    // Returns the multi-indices of the degree d multivariate polynomials with p variables.
    //
    // Calling Sequence
    // a=mmcool_polymultiindex(p,d)
    //
    // Parameters
    // p : a 1-by-1 matrix of doubles, integer value, greater or equal than 1, the number of variables
    // d : a 1-by-1 matrix of doubles, integer value, greater or equal than 0, the total degree of each polynomial
    // a : a nbpoly-by-p matrix of doubles, integer value, greater or equal than 0, the exponents
    //
    // Description
    // Each row a(i,:) represents the i-th polynomial. 
    // The number of rows is the number of degree d polynomials 
    // with p variables.
    // For each row i, a(i,j) is the exponent of the j-th variable, 
    // for j=1,2,...,p.
    // The sum of exponents for each row is lower or equal to d:
    // sum(a(i,:))<=d.
    // The first row is the zero-degree polynomial with 
    // all exponents equal to zero.
    // The rows 2 to p+1 are the first degree polynomials: 
    // all columns are zero, except one entry equal to 1.
    //
    // Examples
    // p=4; // 4 variables
    // d=2; // Total degree = 2
    // a=mmcool_polymultiindex(p,d)
    //
    // References
    // http://en.wikipedia.org/wiki/Monomial
    //
    // O.P. Le Maitre and O.M. Knio. Spectral methods for uncertainty quantification. Springer, 2010. Appendix C Implementation of Product and Moment Formulas, C.2.1 Multi-Index Construction
    [lhs, rhs] = argn();
    apifun_checkrhs ( "polymultiindex" , rhs , 2 : 2 )
    apifun_checklhs ( "polymultiindex" , lhs , 0:1 )
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "polymultiindex" , p ,   "p" ,  1 , "constant" )
    apifun_checktype ( "polymultiindex" , d ,   "d" ,  2 , "constant" )
    // Check size
    apifun_checkscalar ( "polymultiindex" , p ,   "p" ,  1 )
    apifun_checkscalar ( "polymultiindex" , d ,   "d" ,  2 )
    // Check content
    apifun_checkgreq ( "polymultiindex" , p ,   "p" ,  1 , 1 )
    apifun_checkgreq ( "polymultiindex" , d ,   "d" ,  2 , 0 )

    // Zero-th order polynomial
    a(1,1:p)=zeros(1,p)
    // First order polynomials
    if (d>=1) then
        a(2:p+1,1:p)=eye(p,p)
    end
    P=p+1
    pmat=[]
    pmat(1:p,1)=1
    for k=2:d
        L=P
        for i=1:p
            pmat(i,k)=sum(pmat(i:p,k-1))
        end
        for j=1:p
            for m=L-pmat(j,k)+1:L
                P=P+1
                a(P,1:p)=a(m,1:p)
                a(P,j)=a(P,j)+1
            end
        end
    end
endfunction
