// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function boxcoxplot(varargin)
    // Box-Cox plot
    //
    // Calling Sequence
    // boxcoxplot(data)
    // boxcoxplot(data,nbpoints)
    //
    // Parameters
    // data : a m-by-1 matrix of doubles, greater or equal to zero, the data to transform
    // nbpoints : a double, the number of points in the plot (default=100)
    //
    // Description
    // boxcoxplot presents a graphics showing the log-likelihood 
    // depending on the value of lambda. 
    // The optimal value of lambda is shown as a red star in the plot. 
    //
    // Examples
    // data = [0.15 0.09 0.18 0.10 0.05 0.12 0.08];
    // scf();
    // boxcoxplot(data)
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "boxcoxplot" , rhs , 1:2 )
    apifun_checklhs ( "boxcoxplot" , lhs , 0:1 )
    //
    data = varargin(1)
    nbpoints=apifun_argindefault(varargin,2,100)
    //
    // Check Type
    apifun_checktype ( "boxcoxplot" , data , "data" , 1 , "constant" )
    apifun_checktype ( "boxcoxplot" , nbpoints , "nbpoints" , 2 , "constant" )
    //
    // Check Size
    apifun_checkvector ( "boxcoxplot" , data , "data" , 1 )
    apifun_checkscalar ( "boxcoxplot" , nbpoints , "nbpoints" , 2 )
    //
    // Check Content
    apifun_checkgreq ( "boxcoxplot" , data , "data" , 1 , 0 )
    apifun_checkgreq ( "boxcoxplot" , nbpoints , "nbpoints" , 2 , 1 )

    lambda=linspace(-3,3,nbpoints)';
    for i=1:nbpoints
        boxcoxll(i) = boxcoxloglikelihood(data,lambda(i));
    end

    [lambdaopt,loglikeopt] = boxcoxestimate(data)

    // 
    plot(lambda,boxcoxll,"r-")
    plot(lambdaopt,loglikeopt,"r*")
    xlabel("Lambda")
    ylabel("Log-Likelihood")
    title(msprintf("Box-Cox Transformation - Lambda*=%f",lambdaopt))
endfunction

