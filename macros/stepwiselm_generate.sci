// Copyright (C) 2016 - 2017 - Michael Baudin

function [X,multiindices]=stepwiselm_generate(varargin)
    // Generate a linear model from data and a specification
    //
    // Calling Sequence
    // X=stepwiselm_generate(data,modelspec)
    // X=stepwiselm_generate(data,modelspec,selection)
    // [X,multiindices]=stepwiselm_generate(data,modelspec)
    //
    // Parameters
    // data : a samplesize-by-nbinputs matrix of doubles, the input variables where samplesize is the number of experiments and nbinputs is the number of variables
    // modelspec : a 1-by-1 matrix of doubles or string. If modelspec is a double, integer value, greater or equal than 0, the total degree of the polynomial. If modelspec is a string, it can be equal to "constant", "linear", "interactions", "purequadratic", "quadratic" or "polyij". See below for a detailed description of each model.
	// selection : a 1-by-n matrix of doubles, zeros or ones (default selection=ones(1,n), i.e. all variables are selected). Variable X(i) is selected if selection(i)==1.
    // X : a n-by-nbcoeffs matrix of doubles, the input of the generated linear model
    // multiindices : a nbcoeffs-by-nbinputs matrix of doubles, integer value, greater or equal than 0, the exponents
    //
    // Description
    // Generate a model from a specification, with required interactions if necessary. 
    //
    // The variable modelspec allows to choose the total degree of the model. 
    //
    // The available string values of the modelspec are the following. 
    // <itemizedlist>
    // <listitem> <para>
    // "constant" : only a constant term is included (same as modelspec=0)
    // </para> </listitem>
    // <listitem> <para>
    // "linear" : a constant term and each variable are included (same as modelspec=1)
    // </para> </listitem>
    // <listitem> <para>
    // "interactions" : a constant term, each variable and interactions are included (not quadratic term)
    // </para> </listitem>
    // <listitem> <para>
    // "purequadratic" : a constant term, each variable and quadratic terms are included (not interactions)
    // </para> </listitem>
    // <listitem> <para>
    // "quadratic" : a constant term, each variable, interactions and quadratic terms are included (same as modelspec=2)
    // </para> </listitem>
    // <listitem> <para>
    // "polyij" : a polynomial where the maximum degree of each monomial is given. For example, poly402 is for a model with three input variables : the maximum degree for X1 is 4, the maximum degree for X2 is 0, the maximum degree for X3 is 2. 
    // </para> </listitem>
    // </itemizedlist>
    //
    // Authors
    // Copyright (C) 2016 - 2017 - Michael Baudin

    [lhs, rhs] = argn();
    apifun_checkrhs ( "stepwiselm_generate" , rhs , 2 : 3 )
    apifun_checklhs ( "stepwiselm_generate" , lhs , 0:2 )
    //
    data = varargin(1)
    modelspec = varargin(2)
    selection=apifun_argindefault ( varargin,3,[])
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "stepwiselm_generate" , data , "data" ,  1 , "constant" )
    if (typeof(modelspec)~="string") then
        apifun_checktype ( "stepwiselm_generate" , modelspec , "modelspec" ,  2 , "constant" )
    end
    // Check size
    apifun_checkscalar ( "stepwiselm_generate" , modelspec , "modelspec" ,  2 )
    // Check content
    ispolyspec = %f
    if (typeof(modelspec)=="string") then
        splitmodelspec = strsplit(modelspec,"")'
        if (strcat(splitmodelspec(1:4))=="poly") then
            // OK 
            ispolyspec = %t
        else
            apifun_checkoption ( "stepwiselm_generate" , modelspec ,   "modelspec" ,  2 , ["constant","linear","interactions","purequadratic","quadratic"] )
        end
    else
        apifun_checkgreq ( "stepwiselm_generate" , modelspec ,   "modelspec" ,  2 , 0 )
    end
    //
    // 1. Create the required multi-indices
    samplesize = size(data,"r")
    nbinputs=size(data,"c")
    if (typeof(modelspec)=="string") then
        if (ispolyspec) then
            // Check the spec
            degreespec = splitmodelspec(5:$)
            if (size(degreespec,"*")~=nbinputs) then
                errstr = msprintf("%s : The actual number of variables in data is equal to %d, but modelspec specifies %d variables.","stepwiselm_generate",nbinputs,size(degreespec,"*"))
                error(errstr)
            end
            // Convert strings to integers
            maxdegrees = strtod(degreespec)
            integerspec = max(maxdegrees)
        else
            select modelspec
            case "constant" then
                integerspec = 0
            case "linear" then
                integerspec = 1
            case "interactions" then
                integerspec = 2
            case "purequadratic" then
                integerspec = 1
            case "quadratic" then
                integerspec = 2
            else
                errstr = msprintf("%s : Unknown modelspec=%s value.","stepwiselm_generate",modelspec)
                error(errstr)
            end
        end
    else
        integerspec = modelspec
    end
    multiindices=polymultiindex(nbinputs,integerspec)
    // 2. If required, filter or add particular indices
    // If modelspec = "interactions", filter out quadratic terms
    if (modelspec=="interactions") then
        exponentToRemove = 2;
        rowsToKeep = and(multiindices<>exponentToRemove,"c")
        multiindices = multiindices(rowsToKeep,:)
    elseif (modelspec=="purequadratic") then
        // If modelspec = "purequadratic", add quadratic terms
        exponentToAdd = 2;
        quadindices = exponentToAdd*eye(nbinputs,nbinputs)
        multiindices = [multiindices;quadindices]
    elseif (ispolyspec) then
        // Filter the multi-indices matching the required maximum degree
        nbcoeffs = size(multiindices,"r")
        expandedMaxDegrees = repmat(maxdegrees,nbcoeffs,1)
        rowsToKeep = and(multiindices <= expandedMaxDegrees,"c")
        multiindices = multiindices(rowsToKeep,:)
    end
    // 3. Create the associated input matrix
    nbcoeffs = size(multiindices,"r")
    if (selection==[]) then
        // Default case
        selection = ones(1,nbcoeffs)
    end
    indices = find(selection==1)
    nbpredictors = size(indices,"*")
    X = ones(samplesize,nbpredictors);
    // Loop over the selected predictors
    for k=1:nbpredictors
        // i : row indice in multiindices array corresponding to the k-th predictor
        i = indices(k);
        for j=1:nbinputs
            // j : the indice of the input variable
            // exponent_ij : exponent of the i-th coefficient, of the j-th input variable
            exponent_ij = multiindices(i,j);
            if (exponent_ij>0) then
                monom_ij = data(:,j).^exponent_ij;
                X(:,k)=X(:,k).*monom_ij;
            end
        end
    end
    // If required, extract the selected rows in multiindices
    if (selection<>[]) then
        multiindices = multiindices(indices,:)
    end
endfunction
