// Copyright (C) 2012-2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function regressprint(fullstats)
    // Print linear regression
    //
    // Calling Sequence
    //   regressprint(fullstats)
    //
    // Parameters
    // fullstats : a struct, the statistics, see below for details. 
    //
    // Description
    // This function prints the statistics of a linear model with 
    // n independent variables x1, x2, ..., xn 
    // which best fit the data in the least squares sense. 
    // The <literal>fullstats</literal> data structure is the output of the 
    // scidoe_regress or scidoe_multilinreg functions.
    //
    // Examples
    // // Longley.dat contains 1 Response Variable y, 6 Predictor Variables x 
    // // and 16 Observations.
    // // Source : [4]
    // [data,txt] = getdata(24);
    // Y=data(:,1);
    // X=data(:,2:7);
    // [B,bint,r,rint,stats,fullstats] = regress(Y,[ones(Y),X]);
    // regressprint(fullstats)
    //
    // Authors
    // Copyright (C) 2012-2016 - Michael Baudin
    //
    // Bibliography
    // [1] "Introduction to probability and statistics for engineers and scientists.", Third Edition, Sheldon Ross, Elsevier Academic Press, 2004
    // [2] http://en.wikipedia.org/wiki/Linear_regression
    // [3] Octave's regress, William Poetra Yoga Hadisoeseno
    // [4] http://www.itl.nist.gov/div898/strd/lls/data/LINKS/DATA/Longley.dat

    // Check number of input arguments
    [lhs,rhs] = argn();
    apifun_checkrhs("regressprint",rhs,1);
    apifun_checklhs("regressprint",lhs,0:1);
    //
    // Check type
    apifun_checktype("regressprint",fullstats,"fullstats",1,"st");
    //
    // Check size : nothing to do
    //
    // Check content : nothing to do
    //
    // Proceed...
    fmtmax = max(format())
    fmtstr = msprintf("%ds",fmtmax)
    fmttmplate = msprintf("%%%s %%%s %%%s %%%s %%%s",..
    fmtstr,fmtstr,fmtstr,fmtstr,fmtstr)
    mprintf("Analysis of Variance Table\n");
    fmt = "%-12s"+fmttmplate+"\n"
    mprintf(fmt,..
    "Source","Degrees of","Sums of","Mean","F","P");
    mprintf(fmt,..
    "Of Var.","Freedom","Squares","Squares","Stat.","Value");
    mprintf(fmt,..
    "Regression",..
    string(fullstats.RegressionSS),..
    string(fullstats.RegressionDof),..
    string(fullstats.RegressionMean),..
    string(fullstats.F),..
    string(fullstats.pval));
    mprintf(fmt,..
    "Residual",..
    string(fullstats.ResidualSS),..
    string(fullstats.ResidualDof),..
    string(fullstats.ResidualMean),..
    "", ..
    "");
endfunction
