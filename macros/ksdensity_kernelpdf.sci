// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function K=ksdensity_kernelpdf(varargin)
    // Evaluate kernel for PDF
    //  
    // Calling Sequence
    //   K=ksdensity_kernelpdf(u)
    //   K=ksdensity_kernelpdf(u,akernel)
    //
    // Parameters
    // u : a npoints-by-1 matrix of doubles, the points to evaluate the kernel
    // akernel : a 1-by-1 matrix of strings, the type of kernel (default="normal"). Available values are "normal", "biweight", "triangle", "epanechnikov".
    // K : a npoints-by-1 matrix of doubles, the kernel value
    //
    // Description
    // Evaluate the kernel at point u.
    //
    // The available kernels are the following.
    //
    // <itemizedlist>
    //   <listitem><para>
    // If akernel=="normal", 
    //
    // <latex>
    // K(u)= \frac{1}{\sqrt{2\pi}} e^{-\frac{u^2}{2}}
    // </latex>
    //   </para></listitem>
    //   <listitem><para>
    // If akernel=="biweight", 
    //
    // <latex>
    // K(u)= \frac{15}{16}(1-u^2)^2
    // </latex>
    //
    // if abs(u)<1, and zero elsewhere.
    //   </para></listitem>
    //   <listitem><para>
    // If akernel=="triangle", 
    //
    // <latex>
    // K(u)= 1-|u|
    // </latex>
    //
    // if abs(u)<1, and zero elsewhere.
    //   </para></listitem>
    //   <listitem><para>
    // If akernel=="epanechnikov", 
    //
    // <latex>
    // K(u)= \frac{3}{4} (1-u^2)
    // </latex>
    //
    // if abs(u)<1, and zero elsewhere.
    //   </para></listitem>
    // </itemizedlist>
    // 
    // Examples
    // // Plot available kernels 
    // scf();
    // // 
    // subplot(2,2,1)
    // title("Normal")
    // u=linspace(-3,3,100);
    // K=ksdensity_kernelpdf(u,"normal");
    // plot(u,K,"r-")
    // // 
    // subplot(2,2,2)
    // title("Epanechnikov")
    // u=linspace(-1.5,1.5,100);
    // K=ksdensity_kernelpdf(u,"epanechnikov");
    // plot(u,K,"r-")
    // // 
    // subplot(2,2,3)
    // title("Biweight")
    // u=linspace(-1.5,1.5,100);
    // K=ksdensity_kernelpdf(u,"biweight");
    // plot(u,K,"r-")
    // // 
    // subplot(2,2,4)
    // title("Triangle")
    // u=linspace(-1.5,1.5,101);
    // K=ksdensity_kernelpdf(u,"triangle");
    // plot(u,K,"r-")
    // // 
    // // Plot available kernels 
    // scf();
    // xlabel("u");
    // ylabel("K(u)");
    // u=linspace(-1.5,1.5,101);
    // K=ksdensity_kernelpdf(u,"normal");
    // plot(u,K,"m-")
    // K=ksdensity_kernelpdf(u,"epanechnikov");
    // plot(u,K,"b--")
    // K=ksdensity_kernelpdf(u,"biweight");
    // plot(u,K,"r-.")
    // K=ksdensity_kernelpdf(u,"triangle");
    // plot(u,K,"g-")
    // legend(["Normal","Epanechnikov","Biweight","Triangle"]);
    //
    // Authors
    // Copyright (C) 2013 - Michael Baudin
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Kernel_(statistics)#Kernel_functions_in_common_use
    
    [lhs,rhs]=argn()
    apifun_checkrhs ( "ksdensity_kernelpdf" , rhs , 1:2 )
    apifun_checklhs ( "ksdensity_kernelpdf" , lhs , 0:1 )
    //
    u=varargin(1)
    akernel=apifun_argindefault(varargin,2,"normal")
    //
    // Check type
    apifun_checktype ( "ksdensity_kernelpdf" , u , "u" , 1 , "constant" )
    apifun_checktype ( "ksdensity_kernelpdf" , akernel , "kernel" , 2 , "string" )
    //
    // Check size
    // Do not check u : can be matrix or vector
    apifun_checkscalar ( "ksdensity_kernelpdf" , akernel , "kernel" , 2 )
    //
    // Check content
    apifun_checkoption ( "ksdensity_kernelpdf" , akernel , "akernel" , 2 , ..
    ["normal","biweight","triangle","epanechnikov"] )
    // 
    if akernel=="normal" then
        // Normal
        K = exp(-0.5*u.^2)/sqrt(2*%pi);
    elseif akernel=="epanechnikov" then
        // Epanechnikov
        K = (3.0/4.0)*(1-u.^2);
        K(abs(u)>1)=0
    elseif akernel=="biweight" then
        // Biweight
        K = (15.0/16.0)*(1-u.^2).^2;
        K(abs(u)>1)=0
    elseif akernel=="triangle" then
        // Triangular
        K = 1-abs(u);
        K(abs(u)>1)=0
    end
endfunction
