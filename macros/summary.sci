// Copyright (C) 2020 - ESI Group - Clement DAVID
// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function s = summary(x)
    // Descriptive five-number summary of some data
    //
    // Calling Sequence
    //   s = summary(x)
    //
    // Parameters
    //   x : a matrix of doubles, the data to analyze
    //
    // Description
    //   Compute a struct with various descriptive statistics on the input data. These statistics are computed by:
    //   <itemizedlist>
    //     <listitem><para><code>min()</code> stored as min</para></listitem>
    //     <listitem><para><code>quart()</code> stored as firstquartile, median, thirdquartile</para></listitem>
    //     <listitem><para><code>mean()</code> stored as mean</para></listitem>
    //     <listitem><para><code>max()</code> stored as max</para></listitem>
    //   </itemizedlist>
    //
    // Examples
    // summary(1:100)
    // summary(rand(100,1))
    //
    // See also
    //  min
    //  quart
    //  mean
    //  max
    //
    // Authors
    //   Copyright (C) 2014 - Michael Baudin
    //
    // Bibliography
    //   <ulink url="https://en.wikipedia.org/wiki/Five-number_summary">Five-number summary</ulink>
    //
    
    [lhs, rhs] = argn(0)
    if rhs==0 then error(msprintf(gettext("%s: Wrong number of input argument(s): %d expected.\n"),"summary",1)), end

    s.min = min(x)
    q = quart(x)
    s.firstquartile = q(1)
    s.median = q(2)
    s.mean = mean(x)
    s.thirdquartile = q(3)
    s.max = max(x)
endfunction
