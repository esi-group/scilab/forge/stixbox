// Copyright (C) 2013 - 2017 - Michael Baudin
// Copyright (C) 2012 - Scilab Enterprises - Adeline CARNIS
// Copyright (C) 2010 - Samuel Gougeon
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - 1995 - Anders Holtsberg
// Copyright (C) 1998 - 2000 - Mathematique Universite de Paris-Sud - Jean Coursol
// Copyright (C) Bruno Pincon
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [h,edges]=histo(varargin)
    // Plot a  histogram
    //  
    // Calling Sequence
    //   histo(x)
    //   histo(x)
    //   histo(x,n)
    //   histo(...,Name,Value)
    //   h=histo(...)
    //   [h,edges]=histo(...)
    //             
    // Parameters
    // x : a 1-by-p or p-by-1 matrix of doubles, the data
    // n : a 1-by-1 matrix of doubles, the number of bins (default is Sturges rule : n = ceil(log2(p)+1), where p is the size of x. This is Sturges' rule.). If n is not a scalar, it contains the edges of the classes, in increasing order.
    // Name : a string, the option to configure. Can be equal to "Normalization", "BinMethod" or "FaceColor".
    // Value : a string or a matrix of doubles, the value corresponding to the option <literal>Name</literal>.
    // egdes : a 1-by-(n+1) matrix of doubles, the edges of the classes of the histogram
    // h : a 1-by-n matrix of doubles, h(i) is the number of values in x that belong to [edges(i), edges(i+1)[ 
    //
    // Description
    // Compute and plots a histogram of the data in x.
    //
    // Any input argument equal to the empty matrix is replaced by its default value.
    //
    // [h,edges]=histo(...) computes the histogram without plotting.
    //
    // histo(Name,Value) allows to configure the option <literal>Name</literal> 
    // with the value <literal>Value</literal>. 
    //
    // Available options
    //
    // <literal>"Normalization"</literal>: the kind of histogram we compute.
    // <itemizedlist>
    //   <listitem>
    //     <para>
    // <literal>"count"</literal> (default) : each height is equal to the number of 
    // observations in the corresponding bin.
    //     </para>
    //   </listitem>
    //   <listitem>
    //     <para>
    // <literal>"pdf"</literal> : normalize the histogram so that the 
    // integral of the data is equal to 1.
    //     </para>
    //   </listitem>
    //   <listitem>
    //     <para>
    // <literal>"cdf"</literal> : plot the empirical cumulated distribution function
    // of the data.
    //     </para>
    //   </listitem>
    // </itemizedlist>
    //
    // <literal>"BinMethod"</literal>: the method to compute the edges.
    // <itemizedlist>
    //   <listitem>
    //     <para>
    // <literal>"scott"</literal> (default) : the number of bins is computed 
    // with Scott's rule. The bin width is h=3.5*sigma/(p**(1/3)).
    // Scott's rule is good for normally distributed data. 
    // Indeed, it minimizes the integrated mean squared error 
    // of the density estimate.
    //     </para>
    //   </listitem>
    //   <listitem>
    //     <para>
    // <literal>"sturges"</literal> : the number of bins is computed 
    // with Sturges rule : n=ceil(log2(p)+1).
    //     </para>
    //   </listitem>
    //   <listitem>
    //     <para>
    // <literal>"integers"</literal> : consider the data as integers. 
    // The number of bins is the number of unique values in the 
    // data. 
    // Consider the unique values as the centers of the bins. 
    //     </para>
    //   </listitem>
    // </itemizedlist>
    //
    // If the <literal>"BinMethod"</literal> option is used, then 
    // the number of bins <literal>n</literal> cannot be passed to 
    // the histo function, because using both options at the same time 
    // is not consistent. 
    // In other words, the calling sequence 
    // <screen>
    // histo(x,n,"BinMethod",value)
    // </screen>
    // generates an error.
    //
    // <literal>"FaceColor"</literal>: the color of the bars, as a string or a RGB row 
    // vector with 3 entries.
    // <itemizedlist>
    //   <listitem>
    //     <para> Either the long (e.g. "red") or the 
    // short (e.g. "r") color can be specified. 
    // The following colors are available :
    //<screen>
    // "yellow"  "y"
    // "magenta" "m"
    // "cyan"    "c"
    // "red"     "r"
    // "green"   "g"
    // "blue"    "b"
    // "white"   "w"
    // "black"   "k"
    //</screen>
    // Default is "black".
    //     </para>
    //   </listitem>
    //   <listitem>
    //     <para>
    // If the RGB array is provided as a [R,G,B] 
    // row vector, that color is used.
    //     </para>
    //   </listitem>
    // </itemizedlist>
    //
    // Compatibility notes
    //
    // The advantage of the stixbox/histo function over the 
    // Scilab/histplot function is that the number of classes n can be automatically 
    // computed in histo, while it is the first argument of histplot.
    //
    // Examples
    // x=distfun_chi2rnd(3,1000,1);
    // scf();
    // histo(x);
    // xtitle("Chi-Square random numbers","X","Frequency")
    //
    // // Set the number of classes
    // scf();
    // histo(x,10);
    //
    // // Set the edges
    // scf();
    // X=distfun_unifrnd(0,1,1000,1);
    // edges = 0:0.2:1.; 
    // histo(X,edges);
    //
    // // See with "count" Normalization
    // scf(); 
    // subplot(1,2,1)
    // histo(x,"Normalization","count");
    // // See with "pdf" Normalization
    // subplot(1,2,2)
    // histo(x,"Normalization","pdf");
    // x=linspace(0,15);
    // y=distfun_chi2pdf(x,3);
    // plot(x,y);
    //
    // // See with "cdf" Normalization
    // scf(); 
    // histo(x,"Normalization","cdf");
    //
    // // See various colors and styles
    // scf();
    // subplot(1,3,1)
    // histo(x,"FaceColor","b");
    // subplot(1,3,2)
    // histo(x,"FaceColor","blue");
    // subplot(1,3,3)
    // histo(x,"FaceColor",[255,128,128]);
    //
    // // Compute only histogram data 
    // // (do not produce the graphics)
    // x=[0.4112  -0.2789  -0.23  0.0549  -1.0266];
    // [h,edges]=histo(x,4)
    //
    // //
    // // Test BinMethod="integers"
    // scf();
    // pr=0.7;
    // N=10000;
    // R=distfun_geornd(pr,1,N);
    // histo(R,"BinMethod","integers","Normalization","pdf");
    // x=0:6;
    // y = distfun_geopdf(x,pr);
    // plot(x,y,"ro-")
    // legend(["N=1000","PDF"]);
    // xlabel("X")
    // ylabel("P")
    // title("Geometric Distribution pr=0.7")
    //
    // //
    // // Compare Sturges vs Scott
    // data=distfun_normrnd(0,1,1000,1);
    // scf();
    // subplot(1,2,1)
    // histo(data,"BinMethod","scott","Normalization","pdf");
    // title("Scott''s rule")
    // subplot(1,2,2)
    // histo(data,"BinMethod","sturges","Normalization","pdf");
    // title("Sturges''s rule")
    //
    // Authors
    // Copyright (C) 2013 - 2017 - Michael Baudin
    // Copyright (C) 2012 - Scilab Enterprises - Adeline CARNIS
    // Copyright (C) 2010 - Samuel Gougeon
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    // Copyright (C) 1993 - 1995 - Anders Holtsberg
    // Copyright (C) 1998 - 2000 - Mathematique Universite de Paris-Sud - Jean Coursol
    // Copyright (C) Bruno Pincon

    function n=histo_sturges(x)
        p=size(x,"*")
        n=ceil(log2(p)+1)
    endfunction
    function n=histo_scott(x)
        p=size(x,"*")
        sigma=stdev(x)
        h=3.5*sigma/(p**(1/3.))
        xmin=min(x)
        xmax=max(x)
        if (h==0) then
            n=1
        else
            n=ceil((xmax-xmin)/h)
            n=max(n,1)
        end
    endfunction
    function i=histo_searchiteminlist(mylist,myitem)
        // Returns the indice of the first item in list.
        // Return 0 if no item matches
        i=0
        n=length(mylist)
        for j=1:n
            if (mylist(j)==myitem) then
                i=j
                break
            end
        end
    endfunction

    [lhs,rhs]=argn()
    apifun_checkrhs ( "histo" , rhs , 1:7 )
    apifun_checklhs ( "histo" , lhs , 0:2 )
    //
    // 1. Set the defaults
    default=struct(..
    "Normalization","count", ..
    "FaceColor","black",..
    "BinMethod","scott")
    //
    // 2. Manage (key,value) pairs
    if (modulo(rhs,2)==0) then
        options=apifun_keyvaluepairs (default,varargin(3:$))
    else
        options=apifun_keyvaluepairs (default,varargin(2:$))
    end
    //
    // 3. Get parameters
    Normalization = options.Normalization
    FaceColor = options.FaceColor
    BinMethod = options.BinMethod
    //
    x=varargin ( 1 );
    //
    // Check Type
    apifun_checktype ( "histo" , x , "x" , 1 , "constant" )
    apifun_checktype ( "histo" , Normalization , "Normalization" , 3 , "string" )
    apifun_checktype ( "histo" , FaceColor , "FaceColor" , 3 , ["string","constant"] )
    apifun_checktype ( "histo" , BinMethod , "BinMethod" , 3 , "string" )
    //
    // Check Size
    apifun_checkvector ( "histo" , x , "x" , 1 )
    apifun_checkscalar ( "histo" , Normalization , "Normalization" , 3 )
    if (typeof(FaceColor)=="string") then
        apifun_checkscalar ( "histo" , FaceColor , "FaceColor" , 3 )
    else
        apifun_checkvector ( "histo" , FaceColor , "FaceColor" , 3 ,3)
    end
    apifun_checkscalar ( "histo" , BinMethod , "BinMethod" , 3 )
    //
    // Check Content
    avail=["count","pdf","cdf"]
    apifun_checkoption( "histo" , Normalization , "Normalization" , 3 , avail)
    FaceColorShort=["y" "m" "c" "r" "g" "b" "w" "k"]';
    FaceColorLong=[
    "yellow"
    "magenta"
    "cyan"
    "red"
    "green"
    "blue"
    "white"
    "black"
    ];
    if (typeof(FaceColor)=="string") then
        avail=[FaceColorLong;FaceColorShort]
        apifun_checkoption( "histo" , FaceColor , "FaceColor" , 3 , avail)
    else
        apifun_checkrange( "histo" , FaceColor , "FaceColor" , 3 , 0 , 256)
        apifun_checkflint( "histo" , FaceColor , "FaceColor" , 3 )
    end
    avail=["sturges","integers","scott"]
    apifun_checkoption( "histo" , BinMethod , "BinMethod" , 3 , avail)
    //
    if (BinMethod=="sturges") then
        ndefault=histo_sturges(x)
    else
        ndefault=histo_scott(x)
    end
    if (modulo(rhs,2)==1) then
        // histo(x)
        // histo(x,Name1,Value1)
        // histo(x,Name1,Value1,Name2,Value2)
        // key-value pairs are given : n is implicit
        n=ndefault
    else
        // histo(x,n)
        // histo(x,n,Name1,Value1)
        // histo(x,n,Name1,Value1,Name2,Value2)
        // n is given : get it and check it.
        n=apifun_argindefault(varargin,2,ndefault)
        apifun_checktype ( "histo" , n , "n" , 2 , "constant" )
        apifun_checkvector ( "histo" , n , "n" , 2 )
        // Caution : histo(x,n,"BinMethod",value) is forbidden
        i=histo_searchiteminlist(varargin,"BinMethod")
        if (i<>0) then
            errmsg = msprintf(gettext("%s: Both the number of classes %d and the ""BinMethod"" option cannot be set at the same time."), "histo", n);
            error(errmsg)
        end
    end
    //
    // Compute the edges
    if (size(n,"*")>1) then
        // n represents the edges.
        edges = matrix(n,1,-1)   // force row form
        if min(diff(edges)) <= 0 then
            error(msprintf(gettext("%s: Wrong values for input argument #%d: Elements must be in increasing order.\n"),"histo",2))
        end
        n = length(edges)-1
    elseif (BinMethod=="integers") then
        centers=unique(x)
        n=size(centers,"*")
        edges=linspace(centers(1)-0.5,centers($)+0.5,n+1)
    else
        // n is the number of classes.
        minx = min(x);
        maxx = max(x);
        if (minx == maxx) then
            minx = minx - floor(n/2); 
            maxx = maxx + ceil(n/2);
        end
        edges = linspace(minx, maxx, n+1);
    end
    //
    // Compute the histogram
    [ind , h] = dsearch(x, edges);
    //
    // Normalize
    p=size(x,"*");
    if (Normalization=="pdf") then 
        h=h ./ (p *(edges(2:$)-edges(1:$-1)))
    elseif (Normalization=="cdf") then
        h=cumsum(h)/p;
    end
    //
    // If only computing is required, return (do not plot)
    if (lhs==2) then
        return
    end
    //
    // Create the polyline
    //    X = [x1 x1 x2 x2 x2 x3 x3 x3  x4 ...   xn xn+1 xn+1]'
    //    Y = [0  y1 y1 0  y2 y2 0  y3  y3 ... 0 yn yn   0 ]'
    X = [edges(1);edges(1);matrix([1;1;1]*edges(2:n),-1,1);edges(n+1);edges(n+1)];
    // BUG#1885
    // We start the histplot line to %eps rather than 0
    // So when switching to logarithmic mode we do not fall
    // in log(0) special behaviour.
    Y = [matrix([%eps;1;1]*h,-1,1);%eps];
    //
    // Plot
    plot2d(X,Y)
    //
    // Configure FaceColor
    if (typeof(FaceColor)=="string") then
        icolor=find(FaceColor==FaceColorLong)
        if (icolor==[]) then
            // Name is short
            icolor=find(FaceColor==FaceColorShort)
            FaceColor=FaceColorLong(icolor)
        end
        // Name is long
        colorindex=color(FaceColor)
    else
        // Color is RGB
        names=rgb2name(FaceColor(1),FaceColor(2),FaceColor(3))
        colorindex=color(names(1))
    end
    e=gce();
    e.children.foreground=colorindex;
endfunction
