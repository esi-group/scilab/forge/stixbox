// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [lambdaopt,loglikeopt] = boxcoxestimate(varargin)
    // Search optimal lambda
    //
    // Calling Sequence
    // [lambdaopt,loglikeopt] = boxcoxestimate(data)
    // [lambdaopt,loglikeopt] = boxcoxestimate(data,lambda0)
    //
    // Parameters
    //   data : a m-by-1 matrix of doubles, greater or equal to zero, the data to transform
    //   lambda0 : a double, the initial value of lambda (default = 0)
    //   lambdaopt : a double, the optimal lambda
    //   loglikeopt : a double, the optimal value of the log-likelihood
    //
    // Description
    // boxcoxestimate maximizes the log-likelihood of the 
    // Box-Cox transformation using a 1D optimization.
    // The value of lambda which maximizes the log-likelihood 
    // is returned.
    // The log-likelihood is evaluated by the boxcoxloglikelihood function.
    //
    // Examples
    // data = [0.15 0.09 0.18 0.10 0.05 0.12 0.08];
    // [lambdaopt,loglikeopt] = boxcoxestimate(data)
    // [lambdaopt,loglikeopt] = boxcoxestimate(data,2)
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin

    [lhs,rhs]=argn()
    apifun_checkrhs ( "boxcoxestimate" , rhs , 1:2 )
    apifun_checklhs ( "boxcoxestimate" , lhs , 0:6 )
    //
    data=varargin(1)
    lambda0=apifun_argindefault(varargin,2,0.)
    //
    // Check Type
    apifun_checktype ( "boxcoxestimate" , data , "data" , 1 , "constant" )
    apifun_checktype ( "boxcoxestimate" , lambda0 , "lambda0" , 2 , "constant" )
    //
    // Check Size
    apifun_checkvector ( "boxcoxestimate" , data , "data" , 1 )
    apifun_checkscalar ( "boxcoxestimate" , lambda0 , "lambda0" , 1 )
    //
    // Check Content
    apifun_checkgreq ( "boxcoxplot" , data , "data" , 1 , 0 )
    //
    function boxcoxll=internalboxcoxloglike(lambda, data)
        // Reverse the arguments with respect to boxcoxloglikelihood
        // and add a minus sign 
        // in order to derivate the function with numderivative 
        // and to minimize with optim. 
        boxcoxll = - boxcoxloglikelihood(data,lambda)
    endfunction
    function [f, g, ind]=boxcoxcost(lambda, ind, data)
        f = internalboxcoxloglike(lambda, data)
        g = numderivative (list(internalboxcoxloglike,data), lambda);
    endfunction

    [loglikeopt,lambdaopt, gopt, work, iters, evals, err] = optim(list(boxcoxcost,data), lambda0)
    loglikeopt=-loglikeopt
    if (err<>1) then
        warning(msprintf("%s: Optimization may have not completed accurately.\nAfter %d iterations, the gradient is %e.\nPlease use a different value for lambda0=%e\n","boxcoxestimate",iters,gopt,lambda0))
    end
endfunction
