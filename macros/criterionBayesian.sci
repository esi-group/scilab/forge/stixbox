// Copyright (C) 2017 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function c = criterionBayesian(n,logL,m)
    // Bayesian Information Criterion for estimated model
    //
    // Calling Sequence
    //   c = criterionBayesian(n,logL,m)
    //
    // Parameters
    // n : a 1-by-1 matrix of doubles, integer value, the number of variables in the model
    // logL : a 1-by-1 matrix of doubles, integer value, the log-likelihood of the model vs the observations
    // m : a 1-by-1 matrix of doubles, integer value, greater or equal to 1, the number of observations in the model
    // c : a 1-by-1 matrix of doubles, the value of the criterion
    //
    // Description
    // This function computes Akaike's information criterion, i.e. AIC :
    //
    // <screen>
    // c = log(m) * n − 2 * ln (L)
    // </screen>
    //
    // where L is the likelihood of the model vs the observations.
    //
    // If the model has a good fit, the criterion is close to zero 
    // (and positive).
    //
    // Examples
    // // A simple linear regression
    // X = [
    //     57.   
    //     64.   
    //     69.   
    //     82.   
    //     92.   
    //     111.  
    //     114.  
    //     132.  
    //     144.  
    //     146.  
    // ];
    // Y = [
    //     121.  
    //     129.  
    //     140.  
    //     164.  
    //     188.  
    //     217.  
    //     231.  
    //     264.  
    //     289.  
    //     294.  
    // ];
    // [B,bint,r,rint,stats,fullstats] = regress(Y,[ones(X),X])
    // logL = fullstats.LogLikelihood
    // n = size(X,"c")
    // c = criterionAkaike(n,logL)
    //
    // Authors
    // Copyright (C) 2017 - Michael Baudin

    // Check number of input arguments
    [lhs,rhs] = argn();
    apifun_checkrhs("criterionBayesian",rhs,3:3);
    apifun_checklhs("criterionBayesian",lhs,1:1);
    //
    // Check type
    apifun_checktype("criterionBayesian",n,"n",1,"constant");
    apifun_checktype("criterionBayesian",logL,"logL",2,"constant");
    apifun_checktype("criterionBayesian",m,"m",3,"constant");
    //
    // Check size
    apifun_checkscalar("criterionBayesian",n,"n",1);
    apifun_checkscalar("criterionBayesian",logL,"logL",2);
    apifun_checkscalar("criterionBayesian",m,"m",1);
    //
    // Check content
    apifun_checkflint("criterionBayesian",n,"n",1);
    apifun_checkgreq("criterionBayesian",n,"n",1,1);
    apifun_checkflint("criterionBayesian",m,"m",3);
    apifun_checkgreq("criterionBayesian",m,"m",3,1);
    //
    c = log(m) * n - 2 * logL
endfunction
