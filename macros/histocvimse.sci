// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function jhat=histocvimse(x,n)
    // Cross-validation IMSE of an histogram. 
    //
    // Calling Sequence
    // jhat=histocvimse(x,n)
    //
    // Parameters
    // x : a m-by-1 or 1-by-m matrix of doubles, the data
    // n : a nbins-by-1 or 1-by-nbins matrix of doubles, the number of bins in the histogram
    // jhat : a nbins-by-1 or 1-by-nbins matrix of doubles, the leave-one-out cross-validation IIMSE
    //
    // Description
    // Compute the leave-one-out cross-validation 
    // integrated mean square error (IMSE) for the histogram of x with n bins.
    // When this IMSE is minimum, 
    // then the integrated mean square error is minimal.
    //
    // The IMSE has a part which depends on the 
    // histogram and a part which only depends on the 
    // density of the distribution we estimate. 
    // This last part does not depend on the histogram 
    // bin width h. 
    // The function J(h) below is only the part of the 
    // IMSE which depends on the histogram bin width. 
    // This is why the proper name should be partial IMSE.
    //
    // Consider the bin width h=1/m, 
    // where m is the number of observations. 
    // Consider the histogram with n bins. 
    // Let Y(i) the number of observations in the i-th bin. 
    // We compute the IMSE :
    // 
    // <latex>
    // J(h)=\frac{2}{h(m-1)} - \frac{m+1}{h(m-1)} \sum_{i=1}^n p_i^2
    // </latex>
    // 
    // where
    //
    // <latex>
    // p_i = \frac{Y(i)}{m},
    // </latex>
    //
    // for i=1,...,n.
    //
    // Examples
    // m=5; // Number of observations
    // x=distfun_normrnd(0,1,m,1);
    // jhat=histocvimse(x,10)
    //
    // // Prints the Cross-Validation IMSE versus the number of bins
    // m=100; // Number of observations
    // x=distfun_normrnd(0,1,m,1);
    // xlabel("Number of bins");
    // ylabel("Cross-Validation IMSE");
    // jhat=histocvimse(x,1:m);
    // plot((1:m)',jhat,"bo")
    //
    // Bibliography
    // All of non parametric statistics, L. Wasserman, Springer, 2006, chapter 6 "Density estimation", p 129, Theorem 6.15
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin

    [lhs,rhs]=argn()
    apifun_checkrhs ( "histocvimse" , rhs , 2:2 )
    apifun_checklhs ( "histocvimse" , lhs , 0:1 )
    //
    // Check Type
    apifun_checktype ( "histocvimse" , x , "x" , 1 , "constant" )
    apifun_checktype ( "histocvimse" , n , "n" , 2 , "constant" )
    //
    // Check Size
    apifun_checkvector ( "histocvimse" , x , "x" , 1 )
    apifun_checkvector ( "histocvimse" , n , "n" , 2 )
    //
    // Check Content
    apifun_checkgreq ( "histocvimse" , n , "n" , 2 , 1 )
    // 
    // Proceed...

    function jhat=histo_cvmsesingle(n)
        if (n==1) then
            jhat=%inf
            return
        end
        h=1/n // binwidth
        [counts,edges]=histo(x,n)
        // Compute phat : (number of observations in I(i))/m
        // where m is the total number of observations.
        m=size(x,"*")
        phat=counts/m
        jhat=2/(h*(m-1)) - (m+1)*sum(phat.^2)/(h*(m-1))
    endfunction

    jhat=zeros(n)
    bins=size(n,"*")
    for i=1:size(n,"*")
        jhat(i)=histo_cvmsesingle(n(i))
    end
endfunction
