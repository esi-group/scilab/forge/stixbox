// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - 1995 - Anders Holtsberg
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x,txt]=datas1()
txt=['Phosphorus';
'Source: Snedecor, G. W. and Cochran, W. G. (1967),Statistical Methods, (6 Edition), Iowa State University, Ames, Iowa, p. 384.';
'Taken From: Chatterjee and Hadi (1988), p. 82.';
'Dimension:   18 observations on 3 variables';
'Description: An investigation of the source from which corn plants obtain';
'  their phosphorus was carried out. Concentrations of phosphorus';
'  in parts per millions in each of 18 soils were measured.';
'Column       Description';
'  1      Concentrations of inorganic phosphorus in the soil';
'  2      Concentrations of organic phosphorus in the soil';
'  3      Phosphorus content of corn grown in the soil at 20 degrees C'];

 x = [0.4,53,64
  0.4,23,60
  3.1000000000000001,19,71
  0.6,34,61
  4.7000000000000002,24,54
  1.7,65,77
  9.4000000000000004,44,81
  10.1,31,93
  11.6,29,93
  12.6,58,51
  10.9,37,76
  23.100000000000001,46,96
  23.100000000000001,50,77
  21.600000000000001,44,93
  23.100000000000001,56,95
  1.8999999999999999,36,54
  26.800000000000001,58,168
  29.899999999999999,51,99];
endfunction

