// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - 1995 - Anders Holtsberg
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x,txt]=datas20()
  txt=['Paper';
       'Source: ""White paper Recycling"", MIT Technology Review, August-September, 1992';
       'Taken From: J. Milton, J. Arnold (1995), ""Introduction to Probability and statistics"", p 379, McGraw-Hill international editions';
       'Dimensions: 62 observations de 1 variable';
       'Description: Quantite de papiers jetes (en centaines de livres) par an';
       ' par  des salaries de banques (lignes 1 a 32) ou par des';
       ' salaries travaillant dans les autres secteurs (lignes 33 ';
       ' a 62).']

x=[	3.1
	2.9
	3.8
	3.3
	2.7
	3.0
	2.8
	2.5
	2.0
	2.9
	2.1
	2.7
	2.2
	1.8
	1.9
	1.7
	2.6
	2.0
	3.2
	2.4
	2.3
	3.1
	2.1
	3.4
	1.9
	2.5
	2.5
	2.3
	2.3
	1.5
	1.2
	1.7
        6.9
	6.4
	4.7
	4.3
	5.1
	6.3
	5.9
	5.4
	5.2
	5.7
	6.2
	4.2
	5.0
	3.7
	5.1
	5.3
	5.2
	5.1
	5.9
	5.8
	4.9
	4.8
	4.0
	4.0
	5.2
	5.0
	4.1
	3.9
	3.7
	3.4 ];
endfunction
