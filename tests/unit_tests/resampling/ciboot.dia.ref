// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// Estimate a 90% confidence interval for
// the empirical standard deviation
x=[
    2.6393329  
    0.2261208  
    3.4090173  
    2.3128478  
    1.2304635  
    1.8089447  
    1.006141   
    1.2068124  
    2.5974333  
    8.182333   
    2.5544356  
    6.5075578  
    3.3702258  
    0.5537286  
    5.7919164  
    0.8986735  
    4.1781705  
    1.7169013  
    2.3771684  
    2.5330788  
];
s=stdev(x);
ci=ciboot(x,stdev);
ci_expected=[1.277668 2.0583224 2.5557225];
assert_checkequal(size(ci),[1,3]);
assert_checkalmostequal(ci,ci_expected,[],0.2);
//
[ci,y] = ciboot(x,stdev,[],[],1000);
assert_checkalmostequal(ci,ci_expected,[],0.2);
assert_checkequal(size(y),[1 1000]);
//
// Get a covariance
x=[
    0.8147237    0.1883820  
    0.135477     0.5468815  
    0.9057919    0.9928813  
    0.8350086    0.9575068  
    0.1269868    0.9964613  
    0.9688678    0.9648885  
    0.9133759    0.9676949  
    0.2210340    0.1576131  
    0.6323592    0.7258390  
    0.3081671    0.9705928  
    0.0975404    0.9811097  
    0.5472206    0.9571669  
    0.2784982    0.1098618  
];
// Estimate a 90% confidence interval
ci = ciboot(x,cov);
ci_expected=[
   0.0666487    0.1148199    0.1372684  
  -0.0180749    0.0289657    0.0730441  
  -0.0180749    0.0289657    0.0730441  
   0.0531042    0.1262606    0.1642684  
];
assert_checkalmostequal(ci,ci_expected,[],0.1);
// Test all methods
x=[
    2.6393329  
    0.2261208  
    3.4090173  
    2.3128478  
    1.2304635  
    1.8089447  
    1.006141   
    1.2068124  
    2.5974333  
    8.182333   
    2.5544356  
    6.5075578  
    3.3702258  
    0.5537286  
    5.7919164  
    0.8986735  
    4.1781705  
    1.7169013  
    2.3771684  
    2.5330788  
];
ci_expected=[1.277668 2.0583224 2.5557225];
for method=1:6
    ci=ciboot(x,mean,method);
    assert_checkequal(size(ci),[1,3]);
    assert_checkalmostequal(ci,ci_expected,[],2.);
end
// With extra-arguments for T.
x=[
    8.2912182    2.2287117    3.1182913    3.6376971    0.3550386  
    3.0895254    3.8881868    5.3550829    1.3160918    2.114249   
    5.1184774    1.6550287    5.202322     0.5801470    2.0223306  
    3.3856066    1.7038062    6.1548482    3.1132784    4.0819703  
    3.5230084    5.445048     3.5855887    1.7651867    2.6310184  
    3.5019456    6.2341915    2.3523535    2.6677625    4.0841399  
    1.9520921    0.5623473    3.9270753    6.9021254    0.2607122  
    1.6987492    3.3535562    1.0952074    0.9997358    1.595806   
    0.6276884    0.5277657    3.389954     4.3570815    0.9862180  
    1.1637292    0.9759822    1.5614761    7.1913993    3.0743646  
    3.0941288    2.8031232    6.03235      9.1694942    4.2958465  
    5.9817386    8.8669225    6.8084521    3.695747     1.7427705  
    2.5834658    2.4073454    1.6163436    0.2578998    2.3889074  
    5.3550069    0.7228884    1.3846866    0.8572842    0.4604742  
    3.0172649    0.9484140    5.4828775    2.3738533    2.0100617  
    0.8349930    4.8563327    2.0983984    5.888336     1.0033768  
    3.4379126    3.22397      0.4467914    0.5415166    0.5112616  
    2.9298219    1.9448116    5.6819165    2.8990007    0.4742851  
    0.9655807    1.3797477    1.5733388    1.8917438    2.6273832  
    7.5528292    2.8676365    1.5391474    6.4917826    5.0687167  
];
ci=ciboot(x,list(stdev,"r"));
ci_expected=[
    1.5717972    2.1280633    2.658418   
    1.5117654    2.1577854    2.6626327  
    1.4987786    2.0181216    2.6462458  
    1.4618632    2.5759373    2.7403675  
    1.5058157    1.45481      2.6533282  
];
assert_checkalmostequal(ci,ci_expected,[],1.);
