// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Source: 
// Data : http://lib.stat.cmu.edu/datasets/cars.data

// 1. Get data
path=stixbox_getpath();
separator=" ";
conversion="double";
regexpcomments="/#/";
data=csvRead(fullfile(path,"demos","cars.csv"), ..
    separator, [], conversion, [], regexpcomments);
// 2. Extract Miles Per Gallon (column 1), 
// for each country (column 8).
// USA=1, JAPAN=2, EUROPE=3
MPG_USA=thrownan(-data(data(:,8)==1,1));
MPG_JAPAN=thrownan(-data(data(:,8)==2,1));
MPG_EUROPE=thrownan(-data(data(:,8)==3,1));
//
// 3. Make the boxplot with a list
scf();
boxplot(list(MPG_USA,MPG_JAPAN,MPG_EUROPE));
xlabel("Countries");
ylabel("Miles per gallon");
title("USA=1, JAPAN=2, EUROPE=3")
//
// 4. Make the boxplot with a column vector, and groups
X=[MPG_USA;MPG_JAPAN;MPG_EUROPE];
G_USA=repmat("USA",size(MPG_USA));
G_JAPAN=repmat("JAPAN",size(MPG_JAPAN));
G_EUROPE=repmat("EUROPE",size(MPG_EUROPE));
G=[G_USA;G_JAPAN;G_EUROPE];
scf();
boxplot(X,G);
//
// Configure outlier markers
scf();
boxplot(X,G,"symbol","bo");
//
// Configure orientation
scf();
subplot(1,2,1)
boxplot(X,G,"orientation","vertical"); // default
subplot(1,2,2)
boxplot(X,G,"orientation","horizontal");
//
// Make a boxplot for random numbers
x=distfun_normrnd(0,1,1000,25);
scf();
boxplot(x);
title("Normal(0,1) random numbers");
xlabel("Sample Index");
//
// Compare various boxplot for random numbers
x=distfun_normrnd(0,1,1000,25);
scf();
// Vertical and Traditional
subplot(2,2,1)
boxplot(x);
title("Vertical and Traditional");
// Vertical and compact
subplot(2,2,2)
boxplot(x,"plotstyle","compact");
title("Vertical and compact");
// Horizontal and compact
subplot(2,2,3)
boxplot(x,"orientation","horizontal","plotstyle","compact");
title("Horizontal and compact");
// Configure "whisker" width
subplot(2,2,4)
boxplot(x,"whisker",2.);
title("Configure whiskers");
//
// Configure "whisker" width, and "compact" style
scf();
x=distfun_normrnd(0,1,1000,25);
boxplot(x,"whisker",2.,"plotstyle","compact");
title("Normal(0,1) random numbers");
xlabel("Sample Index");
//
// Compare list vs groups on the same window
scf();
subplot(1,2,1)
boxplot(list(MPG_USA,MPG_JAPAN,MPG_EUROPE));
xlabel("Countries");
ylabel("Miles per gallon");
title("USA=1, JAPAN=2, EUROPE=3")
subplot(1,2,2)
boxplot(X,G);
xlabel("Countries");
ylabel("Miles per gallon");
