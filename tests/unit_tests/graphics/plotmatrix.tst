// Copyright (C) 2013 - 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Example 1
// Plot Y versus X
m=1000;
x1=distfun_unifrnd(0,1,m,1);
x2=distfun_unifrnd(0,1,m,1);
x3=distfun_unifrnd(0,1,m,1);
y1=2*x1.*x2+x3;
y2=-3*x1+x2.^2-2*x3;
y3=sin(x1)-3*x2+3*x3;
x=[x1,x2,x3];
y=[y1,y2,y3];
//
xlabels=["X1","X2","X3"];
ylabels=["Y1","Y2","Y3"];
// No labels
scf();
plotmatrix(x,y);
// With labels (Figure 1)
clf();
plotmatrix(x,y,"xlabels",xlabels,"ylabels",ylabels);
// Without XY value labels
clf();
plotmatrix(x,y,"valuelabels",%f);
// Without XY value labels, and XY labels
clf();
plotmatrix(x,y,"valuelabels",%f,..
"xlabels",xlabels,"ylabels",ylabels);
// Set the point size
clf();
plotmatrix(x,y,"ptsize",1);
// With red crosses
clf();
plotmatrix(x,y,"symbol","rx");
//
// http://forge.scilab.org/index.php/p/stixbox/issues/1627/
// ninput=3, noutput=2
clf();
plotmatrix([x1,x2,x3],[y1,y2],"xlabels",["x1","x2","x3"],"ylabels",["y1","y2"]);
//
// ninput=2, noutput=3
clf();
plotmatrix([x1,x2],[y1,y2,y3],"xlabels",["x1","x2"],"ylabels",["y1","y2","y3"]);
//
// Example 2
// Plot Y versus X
m=1000;
x1=distfun_normrnd(0,1,m,1);
x2=distfun_unifrnd(-1,1,m,1);
y1=x1.^2+x2;
y2=-3*x1+x2.^2;
y3=x1-3*exp(x2);
x=[x1,x2];
y=[y1,y2,y3];
//
xlabels=["X1","X2"];
ylabels=["Y1","Y2","Y3"];
// No labels
clf();
plotmatrix(x,y);
// With labels, and red circles
clf();
plotmatrix(x,y,"xlabels",xlabels,"ylabels",ylabels,..
"symbol","ro");
//
// Example 3
// Plot X versus X
m=1000;
x1=distfun_unifrnd(0,1,m,1);
x2=distfun_unifrnd(0,1,m,1);
x3=distfun_unifrnd(0,1,m,1);
y1=2*x1.*x2+x3;
y2=-3*x1+x2.^2-2*x3;
y3=sin(x1)-3*x2+3*x3;
y=[y1,y2,y3];
//
ylabels=["Y1","Y2","Y3"];
// No labels
clf();
plotmatrix(y);
// With labels (Figure 2)
clf();
plotmatrix(y,"xlabels",ylabels);
// With labels, without value labels
clf();
plotmatrix(y,"xlabels",ylabels,"valuelabels",%f);
// With labels, without value labels, with red circles
clf();
plotmatrix(y,"xlabels",ylabels,"valuelabels",%f,..
"symbol","ro");
// With labels, without value labels, with red dots,
// with symbols of size 1
clf();
plotmatrix(y,"xlabels",ylabels,"valuelabels",%f,..
"symbol","r.","ptsize",1);
// With the histogram
clf();
plotmatrix(y,"histogram",%t);
// With the histogram, and the labels
clf();
plotmatrix(y,"histogram",%t,"xlabels",ylabels);
