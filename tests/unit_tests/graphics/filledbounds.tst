// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

n=100;
x=linspace(0,10,n)';
y= x.*sin(x);
sigma=abs(x);
low=y-1.96*sigma;
upp=y+1.96*sigma;
bounds=[low,upp];
//
filledbounds(x,bounds)
plot(x,y,"b-")
legend(["95% Conf. Inter","F"],"in_upper_left");
h=gcf();
delete(h)
//
filledbounds(x,bounds,"foreground",1)
plot(x,y,"b-")
legend(["95% Conf. Inter","F"],"in_upper_left");
h=gcf();
delete(h)
//
filledbounds(x,bounds,"background",3)
plot(x,y,"b-")
legend(["95% Conf. Inter","F"],"in_upper_left");
h=gcf();
delete(h)
