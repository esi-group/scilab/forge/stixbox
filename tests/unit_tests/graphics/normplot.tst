// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// See with normal distribution
x=distfun_normrnd(12,3,50,1);
scf();
normplot(x);
// Change the symbol
clf();
normplot(x,"b.");
// See with uniform distribution
x=distfun_unifrnd(0,1,50,1);
clf();
normplot(x);
// See with chi-square distribution
x=distfun_chi2rnd(2,50,1);
clf();
normplot(x);
