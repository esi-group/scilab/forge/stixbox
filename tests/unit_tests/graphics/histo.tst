// Copyright (C) 2012-2013 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


data=distfun_chi2rnd(3,1000,1);
scf();
histo(data);

//
// Sets the number of classes
clf(); 
histo(data,10);
//
// See "count" Normalization
clf(); 
histo(data,[],"Normalization","count");
// See "pdf" Normalization
clf(); 
histo(data,[],"Normalization","pdf");
//
clf(); 
// See "count" Normalization
subplot(1,3,1)
histo(data,"Normalization","count");
// See "pdf" Normalization
subplot(1,3,2)
histo(data,"Normalization","pdf");
x=linspace(0,15);
y=distfun_chi2pdf(x,3);
plot(x,y);
// See "cdf" Normalization
subplot(1,3,3)
histo(data,"Normalization","cdf");
x=linspace(0,15);
y=distfun_chi2cdf(x,3);
plot(x,y);
//
// See various colors and styles
clf(); 
subplot(2,3,1)
histo(data,[],"FaceColor","black");
subplot(2,3,2)
histo(data,[],"FaceColor","red");
subplot(2,3,3)
histo(data,[],"FaceColor","c");
subplot(2,3,4)
histo(data,[],"FaceColor","y");
subplot(2,3,5)
histo(data,[],"FaceColor",[255,128,128]);
//
X = distfun_unifrnd(0,1,100,1);
edges = 0:0.2:1.; 
clf();
histo(X,edges);
//
// Compute only histogram data
data=[0.4112  -0.2789  -0.23  0.0549  -1.0266];
[h,edges]=histo(data,4);
edges_expected=[-1.0266,-0.66715,-0.3077,0.05175,0.4112];
assert_checkalmostequal(edges,edges_expected);
assert_checkequal(h,[1,0,2,2]);
//
// Sets the number of classes
data=[0.4112  -0.2789  -0.23  0.0549  -1.0266];
[h,edge]=histo(data,10);
assert_checkequal ( typeof(h) , "constant" );
assert_checkequal ( size(h) , [1 10] );
assert_checkequal ( and(h>=0) , %t );
assert_checkequal ( typeof(edge) , "constant" );
assert_checkequal ( size(edge) , [1 11] );
//
// Test BinMethod="integers"
R=[1 2 0 1 2 3 1];
clf();
histo(R,"BinMethod","integers");
[h,edges]=histo(R,"BinMethod","integers");
//
clf();
R=[1 2 0 1 2 3 1];
histo(R,"BinMethod","integers","Normalization","pdf");
//
// Test BinMethod="integers"
clf();
pr=0.7;
N=10000;
R=distfun_geornd(pr,1,N);
histo(R,"BinMethod","integers","Normalization","pdf");
x=0:6;
y = distfun_geopdf(x,pr);
plot(x,y,"ro-")
legend(["N=1000","PDF"]);
xlabel("X")
ylabel("P")
title("Geometric Distribution pr=0.7")
//
// Compare Sturges vs Scott
data=distfun_normrnd(0,1,1000,1);
clf();
subplot(1,2,1)
histo(data,"BinMethod","scott","Normalization","pdf");
title("Scott''s rule")
subplot(1,2,2)
histo(data,"BinMethod","sturges","Normalization","pdf");
title("Sturges''s rule")


// https://forge.scilab.org/index.php/p/stixbox/issues/1665/
x=[0,0,0,0];
scf();
histo(x);
for binmethod=["scott","sturges"]
    clf();
    histo(x,"BinMethod",binmethod);
end

// tests with all parameters
histo(1:10,"Normalization","pdf","FaceColor","red","BinMethod","sturges");

