// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

x=510;
y=betaln(x,x);
assert_checkalmostequal ( y , -708.8616 ,1.e-7);
