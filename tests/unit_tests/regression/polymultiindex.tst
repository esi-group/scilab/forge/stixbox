// Copyright (C) 2017 - Michael Baudin

p=1;
d=0;
a=polymultiindex(p,d);
expected = [
    0.
];
assert_checkequal(a,expected);
//
p=2;
d=0;
a=polymultiindex(p,d);
expected = [
    0.    0.
];
assert_checkequal(a,expected);
//
p=3;
d=0;
a=polymultiindex(p,d);
expected = [
    0.    0.    0.  
];
assert_checkequal(a,expected);
//
p=3;
d=1;
a=polymultiindex(p,d);
expected = [
    0.    0.    0.  
    1.    0.    0.  
    0.    1.    0.  
    0.    0.    1.  
];
assert_checkequal(a,expected);
//
p=3;
d=2;
a=polymultiindex(p,d);
expected = [
    0.    0.    0.  
    1.    0.    0.  
    0.    1.    0.  
    0.    0.    1.  
    2.    0.    0.  
    1.    1.    0.  
    1.    0.    1.  
    0.    2.    0.  
    0.    1.    1.  
    0.    0.    2.  
];
assert_checkequal(a,expected);
//
p=3;
d=3;
a=polymultiindex(p,d);
expected = [
    0.    0.    0.  
    1.    0.    0.  
    0.    1.    0.  
    0.    0.    1.  
    2.    0.    0.  
    1.    1.    0.  
    1.    0.    1.  
    0.    2.    0.  
    0.    1.    1.  
    0.    0.    2.  
    3.    0.    0.  
    2.    1.    0.  
    2.    0.    1.  
    1.    2.    0.  
    1.    1.    1.  
    1.    0.    2.  
    0.    3.    0.  
    0.    2.    1.  
    0.    1.    2.  
    0.    0.    3.  
];
assert_checkequal(a,expected);
