// Copyright (C) 2016 - 2017 - Michael Baudin

nbinputs = 4;
inputlabels = "X" + string(1:nbinputs);
multiindices = [
    0.    0.    0.    0.  
    1.    0.    0.    0.  
    0.    1.    0.    0.  
    0.    0.    1.    0.  
    0.    0.    0.    1.  
    2.    0.    0.    0.  
    1.    1.    0.    0.  
    1.    0.    1.    0.  
    1.    0.    0.    1.  
    0.    2.    0.    0.  
    0.    1.    1.    0.  
    0.    1.    0.    1.  
    0.    0.    2.    0.  
    0.    0.    1.    1.  
    0.    0.    0.    2.  
];
str = stepwiselm_print(multiindices,inputlabels);
expected = [
"1"
"(X1)"
"(X2)"
"(X3)"
"(X4)"
"(X1)^2"
"(X1)*(X2)"
"(X1)*(X3)"
"(X1)*(X4)"
"(X2)^2"
"(X2)*(X3)"
"(X2)*(X4)"
"(X3)^2"
"(X3)*(X4)"
"(X4)^2"
];
assert_checkequal(str,expected)
