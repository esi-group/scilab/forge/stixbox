// Copyright (C) 2012-2016 - Michael Baudin
// Copyright (C) 2012 - Maria Christopoulou 
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->

// We use dataset provided by NIST in
// http://www.itl.nist.gov/div898/strd/lls/data/LINKS/DATA/Longley.dat
// Longley.dat contains 1 Response Variable y, 6 Predictor Variables x 
// and 16 Observations.
// The model used is Y = B0 + B1*x1 + B2*x2 +...Bn*xn
[data,txt] = getdata(24);
Y=data(:,1);
X=data(:,2:7);
[B,bint,r,rint,stats,fullstats] = regress(Y,[ones(Y),X]);
regressprint(fullstats);
