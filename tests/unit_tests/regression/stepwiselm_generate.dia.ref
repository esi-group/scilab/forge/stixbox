// Copyright (C) 2016 - 2017 - Michael Baudin
xmin = 0;
xmax = 4;
samplesize = 45;
nbinputs = 6;
data = distfun_unifrnd(xmin,xmax,[samplesize,nbinputs]);
// Test degree 0
// Constant term only : 1
inputmodel=0; 
X=stepwiselm_generate(data,inputmodel);
assert_checkequal(size(X),[samplesize,1]);
assert_checkequal(X(:,1),ones(samplesize,1));
// Test multi-indices
[X,multiindices]=stepwiselm_generate(data,inputmodel);
expected = [
    0.    0.    0.    0.    0.    0.  
];
assert_checkequal(multiindices,expected);
// Test degree 1
// Linear terms only : 1, X1, X2, etc...
inputmodel=1; 
X=stepwiselm_generate(data,inputmodel);
assert_checkequal(size(X),[samplesize,7]);
assert_checkequal(X(:,1),ones(samplesize,1));
for i = 1:nbinputs
    assert_checkequal(X(:,i+1),data(:,i));
end
// Test multi-indices
[X,multiindices]=stepwiselm_generate(data,inputmodel);
expected = [
    0.    0.    0.    0.    0.    0.  
    1.    0.    0.    0.    0.    0.  
    0.    1.    0.    0.    0.    0.  
    0.    0.    1.    0.    0.    0.  
    0.    0.    0.    1.    0.    0.  
    0.    0.    0.    0.    1.    0.  
    0.    0.    0.    0.    0.    1.  
];
assert_checkequal(multiindices,expected);
//
// Test degree 2
// Linear terms, interactions (and quadratic) : 
// 1, X1, X1*X2, etc...
inputmodel=2; 
X=stepwiselm_generate(data,inputmodel);
assert_checkequal(size(X),[samplesize,28]);
assert_checkequal(X(:,1),ones(samplesize,1));
for i = 1:nbinputs
    assert_checkequal(X(:,i+1),data(:,i));
end
assert_checkequal(X(:,nbinputs+2),data(:,1).^2);
assert_checkequal(X(:,nbinputs+3),data(:,1).*data(:,2));
assert_checkequal(X(:,2*nbinputs+2),data(:,2).^2);
assert_checkequal(X(:,$),data(:,$).^2);
// Test multi-indices
[X,multiindices]=stepwiselm_generate(data,inputmodel);
expected = [
    0.    0.    0.    0.    0.    0.  
    1.    0.    0.    0.    0.    0.  
    0.    1.    0.    0.    0.    0.  
    0.    0.    1.    0.    0.    0.  
    0.    0.    0.    1.    0.    0.  
    0.    0.    0.    0.    1.    0.  
    0.    0.    0.    0.    0.    1.  
    2.    0.    0.    0.    0.    0.  
    1.    1.    0.    0.    0.    0.  
    1.    0.    1.    0.    0.    0.  
    1.    0.    0.    1.    0.    0.  
    1.    0.    0.    0.    1.    0.  
    1.    0.    0.    0.    0.    1.  
    0.    2.    0.    0.    0.    0.  
    0.    1.    1.    0.    0.    0.  
    0.    1.    0.    1.    0.    0.  
    0.    1.    0.    0.    1.    0.  
    0.    1.    0.    0.    0.    1.  
    0.    0.    2.    0.    0.    0.  
    0.    0.    1.    1.    0.    0.  
    0.    0.    1.    0.    1.    0.  
    0.    0.    1.    0.    0.    1.  
    0.    0.    0.    2.    0.    0.  
    0.    0.    0.    1.    1.    0.  
    0.    0.    0.    1.    0.    1.  
    0.    0.    0.    0.    2.    0.  
    0.    0.    0.    0.    1.    1.  
    0.    0.    0.    0.    0.    2.  
];
assert_checkequal(multiindices,expected);
//
// Test degree 3
// Linear terms, interactions and order 3 interactions : 
// 1, X1, X1*X2, X1*X2*X3, etc...
inputmodel=3; 
X=stepwiselm_generate(data,inputmodel);
assert_checkequal(size(X),[samplesize,84]);
assert_checkequal(X(:,1),ones(samplesize,1));
for i = 1:nbinputs
    assert_checkequal(X(:,i+1),data(:,i));
end
assert_checkequal(X(:,nbinputs+2),data(:,1).^2);
assert_checkequal(X(:,nbinputs+3),data(:,1).*data(:,2));
assert_checkequal(X(:,2*nbinputs+2),data(:,2).^2);
assert_checkequal(X(:,$),data(:,$).^3);
//
// Test "constant"
[X,multiindices]=stepwiselm_generate(data,"constant");
[X_ref,multiindices_ref]=stepwiselm_generate(data,0);
assert_checkequal(X,X_ref);
assert_checkequal(multiindices,multiindices_ref);
//
// Test "linear"
[X,multiindices]=stepwiselm_generate(data,"linear");
[X_ref,multiindices_ref]=stepwiselm_generate(data,1);
assert_checkequal(X,X_ref);
assert_checkequal(multiindices,multiindices_ref);
//
// Test "interactions"
[X,multiindices]=stepwiselm_generate(data,"interactions");
assert_checkequal(size(X),[samplesize,22]);
assert_checkequal(X(:,1),ones(samplesize,1));
for i = 1:nbinputs
    assert_checkequal(X(:,i+1),data(:,i));
end
// Check that there is no quadratic term
quadindices = find(multiindices==2);
assert_checkequal(quadindices,[]);
//
// Test "purequadratic"
[X,multiindices]=stepwiselm_generate(data,"purequadratic");
assert_checkequal(size(X),[samplesize,13]);
assert_checkequal(X(:,1),ones(samplesize,1));
for i = 1:nbinputs
    assert_checkequal(X(:,i+1),data(:,i));
end
for i = 1:nbinputs
    assert_checkequal(X(:,nbinputs+1+i),data(:,i).^2);
end
//
// Test "quadratic"
[X,multiindices]=stepwiselm_generate(data,"quadratic");
[X_ref,multiindices_ref]=stepwiselm_generate(data,2);
assert_checkequal(X,X_ref);
assert_checkequal(multiindices,multiindices_ref);
//
// Test "poly"
// Reduce number of variables to be able to check all.
xmin = 0;
xmax = 4;
samplesize = 45;
nbinputs = 4;
data = distfun_unifrnd(xmin,xmax,[samplesize,nbinputs]);
[X,multiindices]=stepwiselm_generate(data,"poly2031");
expected = [
    0.    0.    0.    0.  
    1.    0.    0.    0.  
    0.    0.    1.    0.  
    0.    0.    0.    1.  
    2.    0.    0.    0.  
    1.    0.    1.    0.  
    1.    0.    0.    1.  
    0.    0.    2.    0.  
    0.    0.    1.    1.  
    2.    0.    1.    0.  
    2.    0.    0.    1.  
    1.    0.    2.    0.  
    1.    0.    1.    1.  
    0.    0.    3.    0.  
    0.    0.    2.    1.  
];
assert_checkequal(multiindices,expected);
assert_checkequal(max(multiindices,"r"),[2 0 3 1]);
assert_checkequal(size(X),[samplesize,15]);
//
// Test generate with selection
xmin = 0;
xmax = 4;
samplesize = 15;
nbinputs = 3;
data = distfun_unifrnd(xmin,xmax,[samplesize,nbinputs]);
inputmodel=2; 
// Without selection (only for reference)
[X_full,multiindices_full]=stepwiselm_generate(data,inputmodel);
// With selection
selection=ones(1,10);
// Only keep linear and pure quadratic terms
selection([6,7,9])=0;
[X,multiindices]=stepwiselm_generate(data,inputmodel,selection);
indices=find(selection==1);
nbpredictors = size(indices,"*");
assert_checkequal(nbpredictors,7);
assert_checkequal(size(multiindices),[nbpredictors,nbinputs]);
assert_checkequal(size(X),[samplesize,nbpredictors]);
// multiindices is made of selected rows of multiindices_full
assert_checkequal(multiindices_full(indices,:),multiindices);
// X is made of selected columns of X_full
assert_checkequal(X_full(:,indices),X);
