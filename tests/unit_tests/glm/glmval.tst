// Copyright (C) 2017 - Michael Baudin

// <-- ENGLISH IMPOSED -->

// http://fr.mathworks.com/help/stats/glmfit.html
// http://eric.univ-lyon2.fr/~ricco/cours/slides/regression_logistique.pdf

age=[50,49,46,49,62,35,67,65,47,58,57,59,44,41,54,52,57,50,44,49];
taux=[126,126,144,139,154,156,160,140,143,165,115,145,175,153,152,169,168,158,170,171];
angine=[1,0,0,0,1,1,0,0,0,0,1,0,0,0,0,0,1,0,0,0];
coeur=[1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
X=[age',taux',angine'];
y=coeur';

// Evalue un ajustement précédent
// Ref : Ricco, page 14.
b=[14.494;-0.1256;-0.06356;1.779];
yhat =glmval(b,X);
yref=[
    0.879
    0.582
    0.392
    0.378
    0.213
    0.877
    0.016
    0.071
    0.378
    0.036
    0.858
    0.106
    0.104
    0.406
    0.124
    0.058
    0.173
    0.138
    0.137
    0.077
];
//
assert_checkalmostequal(yhat,yref,[],1.e-2);
