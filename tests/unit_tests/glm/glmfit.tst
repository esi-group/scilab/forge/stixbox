// Copyright (C) 2017 - Michael Baudin

// <-- ENGLISH IMPOSED -->

// http://fr.mathworks.com/help/stats/glmfit.html
// http://eric.univ-lyon2.fr/~ricco/cours/slides/regression_logistique.pdf

age=[50,49,46,49,62,35,67,65,47,58,57,59,44,41,54,52,57,50,44,49];
taux=[126,126,144,139,154,156,160,140,143,165,115,145,175,153,152,169,168,158,170,171];
angine=[1,0,0,0,1,1,0,0,0,0,1,0,0,0,0,0,1,0,0,0];
coeur=[1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
X=[age',taux',angine'];
y=coeur';

// Ajustement
b = glmfit(X,y);
bref=[14.494;-0.126;-0.064;1.779];
// Ref : Ricco, page 14
assert_checkalmostequal(b,bref,[],1.e-3);
