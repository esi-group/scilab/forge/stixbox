// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

data = [0.15 0.09 0.18 0.10 0.05 0.12 0.08]';
lambda=0.7;
transdat=boxcox(data,lambda);
exact=[
  -1.0499843  
  -1.1637996  
  -0.9984484  
  -1.143534   
  -1.2531103  
  -1.1047327  
  -1.1847539  
];
assert_checkalmostequal(transdat,exact,1.e-6);
//
data = [
0.15	0.09	0.18	0.10	0.05	0.12	0.08
0.05	0.08	0.10	0.07	0.02	0.01	0.10
0.10	0.10	0.02	0.10	0.01	0.40	0.10
0.05	0.03	0.05	0.15	0.10	0.15	0.09
0.08	0.18	0.10	0.20	0.11	0.30	0.02
0.20	0.20	0.30	0.30	0.40	0.30	0.05
];
data=data(:);
[transdat,lambda] = boxcox(data);
lambdaexact=0.275937278987;
assert_checkalmostequal(lambda,lambdaexact);
assert_checkequal(size(transdat),size(data));
// Just check the first, the last
assert_checkalmostequal(transdat(1),-1.476960340238);
assert_checkalmostequal(transdat($),-2.038436392769);
//
// Check cancellation
transdat=boxcox(0.15,1.e-20);
exact=-1.897119984885881302; // Exact up to 17 digits
assert_checkalmostequal(transdat,exact);

