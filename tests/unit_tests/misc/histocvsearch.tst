// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

m=100; // Number of observations
x=distfun_normrnd(0,1,m,1);
[n,jhat]=histocvsearch(x);
assert_checkequal(size(n),[1 1]);
assert_checkequal(size(jhat),[1 1]);

scf();
histo(x,n);

// Exact check
x=[
   1.0292466   1.9923854   1.1392149   0.6952158  -0.8595698  
   0.2253331  -1.2060957  -0.7302189  -0.6157808   2.1463811  
   0.2089027   0.7704313   0.3610298   1.4304761   0.6028264  
  -0.7697560   0.2377067  -0.3893915   0.0180007   1.5192905  
   1.6396997   0.1673013  -0.0686898  -1.0005075   0.5022426  
   1.0917421  -1.2927853  -0.8043033   1.7860757  -1.4347724  
  -0.4920649   1.464274    0.2572927   0.8470950  -0.2492009  
   0.5508951   0.5326127   1.8552999  -0.1685566  -0.0379391  
   0.7987102   1.0111957  -2.5740748   0.9696415  -0.4295839  
  -0.6347240   0.3468577   1.0819887  -0.5564755  -0.6354395  
   1.0746745   1.3604425   0.0827611   0.7302292  -0.0086263  
  -0.2919032  -0.0839433   1.4243679  -0.4389311  -1.2544099  
  -0.6069184   1.2492641   0.7148571  -0.7627728   1.8128714  
  -0.3667097   1.1648004  -0.5314478   1.2953156  -0.9395702  
  -0.1154117  -1.0254197   1.5974223   0.3221484  -0.3031     
  -0.0701959  -0.3312574   3.169585    0.8237859  -0.2248182  
  -1.1660829   2.0334093  -0.0831540  -0.5740437  -1.9458405  
  -0.8879414   0.5979629  -1.2642309   0.3067198  -0.9257153  
  -1.3250882  -2.3130148  -1.8491473  -0.9033062  -0.6467482  
   0.6385773   1.0154305   2.9042896  -0.7496884  -1.5363012  
];
x=x(:);
[n,jhat]=histocvsearch(x);
assert_checkequal(n,6);
assert_checkalmostequal(jhat,-1.3686909091);
//
nstep=2;
[n,jhat]=histocvsearch(x,nstep);
assert_checkequal(n,9);
assert_checkalmostequal(jhat,-1.3662363636);
//
nmax=50;
[n,jhat]=histocvsearch(x,[],nmax);
assert_checkequal(n,6);
assert_checkalmostequal(jhat,-1.3686909091);
