// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

x=[0.4112  -0.2789  -0.23  0.0549  -1.0266];
jhat=histocvimse(x,2);
assert_checkalmostequal(jhat,-1.04);
//
// Compute several IMSEs at once
x=[0.4112  -0.2789  -0.23  0.0549  -1.0266];
jhat=histocvimse(x,[2,3]);
assert_checkalmostequal(jhat,[-1.04  -0.12]);
