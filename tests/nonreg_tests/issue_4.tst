// Copyright (C) 2024 - 3DS - Vincent COUVERT

// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->

// Non-regression test for issue #4
// https://gitlab.com/scilab/forge/stixbox/-/issues/4

x = grand(1, 10, "nor", 10, 0.5);
y = grand(1, 10, "nor", 10, 0.5);
[d, p]=kstwo(x, y);
assert_checkequal(d, 1);
assert_checkequal(p, 0);