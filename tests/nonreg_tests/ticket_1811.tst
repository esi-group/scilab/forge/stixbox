// Copyright (C) 2020 - ESI Group - Clement DAVID


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->
// <-- NO CHECK REF -->

//1. Perform a linear regression to get confidence intervals with confidence level .95:
x=1:20;
x=x';
y=1+x+grand(20,1,'nor',0,1);
X=[ones(x),x];
[B,I]=regress(y,X, .05)

//2. Get confidence intervals with confidence level .90:
[B2,I2]=regress(y,X, .1)

//Expected result: I and I2 should be different
assert_checkalmostequal(B, B2);
assert_checktrue(I <> I2);
assert_checkalmostequal(I, I2, [], 0.25);
