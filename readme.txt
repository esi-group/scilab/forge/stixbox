Stixbox

Purpose
-------

Stixbox is a statistics toolbox which provides datasets, 
statistical tests and plotting facilities. 

Features
--------

 * Box-Cox
   * boxcox : Box-Cox transformation
   * boxcoxestimate : Search optimal lambda
   * boxcoxinverse : Inverse Box-Cox transformation
   * boxcoxloglikelihood : Box-Cox log-likelihood
   * boxcoxplot : Box-Cox plot
 * Datasets
   * getdata : Famous datasets 
   * getdatanumber : Number of available datasets
 * Graphics
   * Tutorial : Demos of the graphics.
   * boxplot : Draw a box plot
   * bubblechart : Plot a bubble chart
   * bubblematrix : Plot a bubble chart
   * filledbounds : Fill the area between two curves
   * histo : Plot a histogram
   * identify : Identify points on a plot with mouse clicks
   * plotmatrix : Plot an X vx Y scatter plot matrix
   * plotsym : Plot with symbols
   * qqnorm : Normal probability paper
   * qqplot : Create a QQ-plot
   * stairs : Stairstep graph
 * Logistic Regression
   * lodds : Log odds function 
   * loddsinv : inverse of log odd 
   * logitfit : Fit a logistic regression model 
 * Miscellaneous
   * betaln : Logarithm of beta function 
   * histocvimse : Cross-validation IMSE of an histogram.
   * histocvsearch : Search optimal CV number of bins in a histogram.
   * ksdensity : Kernel smoothing density estimate
   * ksdensity_kernelcdf : Evaluate kernel for CDF
   * ksdensity_kernelpdf : Evaluate kernel for PDF
   * quantile : Empirical quantile (percentile). 
   * stixbox_getpath : Returns the path to the current module.
 * Regression
   * cmpmod : Compare linear submodel versus larger one
   * lsselect : Select a predictor subset for regression
   * regress : Multiple linear regression
   * regressprint : Print linear regression
 * Resampling
   * T and extra arguments : How to use extra-arguments in T.
   * ciboot : Bootstrap confidence intervals
   * covboot : Bootstrap estimate of the variance of a parameter estimate.
   * covjack : Jackknife estimate of the variance of a parameter estimate.
   * rboot : Simulate a bootstrap resample
   * stdboot : Bootstrap estimate of the parameter standard deviation.
   * stdjack : Jackknife estimate of the standard deviation of a parameter estimate.
 * Tests, confidence intervals and model estimation
   * ciquant : Nonparametric confidence interval for quantile
   * kstwo : Kolmogorov-Smirnov statistic from two samples
   * test1b : Bootstrap test that mean equals zero
   * test1n : Test that mean equals zero (Normal)
   * test1r : Test for median equals 0 using rank test
   * test2n : Tests two normal samples with equal variance
   * test2r : Test location equality of two samples using rank test

Dependencies 
------------

 * This module depends on Scilab (v>=5.5).
 * This module depends on the "apifun" module.
 * This module depends on the "helptbx" module.
 * This module depends on the "distfun" module (v>=0.8).
 * This module depends on the "makematrix" module (v>=0.4)

TODO
-----

 * Create unit tests for all functions.
 * Add examples where necessary.
 * Put the help content into the Scilab macros, and 
   generate the help with help_from_sci.
 * Add a warnobsolete for the std functions.
 * Merge the stixbox/cov and Scilab/covar functions.
 * Use apifun where appropriate.
 * Update the stixtest.dem script. 
   Display a listbox with all the available distribution 
   functions instead of letting the user type "Enter" in the console.

Bibliography
------------

http://www.maths.lth.se/matstat/stixbox/

Authors
-------

* Copyright (C) 2012-2017 - Michael Baudin
* Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
* Copyright (C) 2008 - INRIA - Allan CORNET
* Copyright (C) 2006 - INRIA - Serge Steer
* Copyright (C) 2001 - ENPC - Jean-Philippe Chancelier
* Copyright (C) Paris Sud University.
* Copyright (C) INRIA - Maurice Goursat
* Copyright (C) 2000 - Anders Holtsberg

The Scilab toolbox stixbox is derived from the matlab 
stixbox version 1.1.

Licence
-------

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
