// ====================================================================
// Copyright DIGITEO 2008 - 2011 - Allan CORNET
// ====================================================================
function build_help()
  help_dir = get_absolute_file_path('builder_help.sce');
  tbx_builder_help_lang("en_US", help_dir);
endfunction
// ====================================================================
build_help();
clear build_help;
// ====================================================================

