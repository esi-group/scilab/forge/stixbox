<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from stdboot.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="stdboot" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>stdboot</refname>
    <refpurpose>Bootstrap estimate of the parameter standard deviation.</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   s=stdboot(x,T)
   s=stdboot(x,T,b)
   [s,y]=stdboot(...)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x :</term>
      <listitem><para> a matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>T :</term>
      <listitem><para> a function or a list, the function which computes the empirical estimate from x.</para></listitem></varlistentry>
   <varlistentry><term>b :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the number of bootstrap resamples (default b=200)</para></listitem></varlistentry>
   <varlistentry><term>s :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the estimate of the standard deviation</para></listitem></varlistentry>
   <varlistentry><term>y :</term>
      <listitem><para> a 1-by-b matrix of doubles, the values of T of the resamples</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Jackknife estimate of the standard deviation of the parameter
estimate T(x).
   </para>
   <para>
The function T must have the following header:
<screen>
p=T(x)
</screen>
where <literal>x</literal> is the sample or the resample
and <literal>p</literal> is a m-by-1 matrix of doubles.
In the case where the parameter estimate has a more general
shape (e.g. 2-by-2), the shape of <literal>p</literal> is reshaped
into a column vector with <literal>m</literal> components.
   </para>
   <para>
See "T and extra arguments" for details on how to pass extra-arguments
to T.
   </para>
   <para>
The function is equal to
<screen>
sqrt(diag(covboot(x,T)))
</screen>
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Estimate the standard deviation of the
// empirical mean
n = 20;
x=distfun_chi2rnd(3,n,1);
m=mean(x) // Empirical mean
s=stdev(x)/sqrt(n) // Standard error for the mean
s=stdboot(x,mean) // Standard error with bootstrap
// Get y
[s,y]=stdboot(x,mean);
size(y)
// Set the number of resamples
[s,y]=stdboot(x,mean,1000);
size(y)
// Estimate the standard deviation of the median
m=median(x) // Empirical median
s=stdboot(x,median)

// With extra-arguments for T.
x=distfun_chi2rnd(3,20,5);
mean(x,"r")
s=stdboot(x,list(mean,"r"))

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2013 - 2014 - Michael Baudin</member>
   <member>Copyright (C) 2010 - DIGITEO - Michael Baudin</member>
   <member>Copyright (C) 1993 - 1995 - Anders Holtsberg</member>
   </simplelist>
</refsection>
</refentry>
